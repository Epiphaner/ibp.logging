﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Output
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 10-31-2018
// ***********************************************************************
// <copyright file="OutputLogConfiguration.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Configuration {
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// Contains a configuration for logging to the Output.
	/// </summary>
	public class OutputLogConfiguration : LogConfigurationBase {
	}
}
