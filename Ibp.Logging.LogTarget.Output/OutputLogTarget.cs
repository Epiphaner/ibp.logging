﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Output
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 10-31-2018
// ***********************************************************************
// <copyright file="OutputLogTarget.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.LogTargets {
	using System.Collections.Generic;
	using System.Diagnostics;
	using Ibp.Logging.Configuration;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.Core.LogTargets;

	/// <summary>
	/// Simply logs the entries to the output of the application.
	/// </summary>
	public class OutputLogTarget : LogTarget<OutputLogConfiguration> {
		/// <summary>
		/// Initializes a new instance of the <see cref="OutputLogTarget"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to use.</param>
		public OutputLogTarget(OutputLogConfiguration configuration)
			: base(configuration) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="OutputLogTarget"/> class.
		/// Uses the default configuration.
		/// </summary>
		public OutputLogTarget()
			: this(new OutputLogConfiguration()) { }

		/// <inheritdoc/>
		public override void TestConfiguration(ConfigurationTestResults testResults)
		{
			// No configuration that needs testing.
		}

		/// <inheritdoc/>
		public override void InitializeTarget()
		{
			// Nothing to initialize.
		}

		/// <summary>
		/// Finalizes this instance.
		/// Finalization performs all one-off actions that should be performed after logging is done.
		/// </summary>
		public override void FinalizeTarget()
		{
			// No finalization needed.
		}

		/// <inheritdoc/>
		protected sealed override void Log(List<LogEntry> entries) {
			foreach (LogEntry entry in entries) {
				Debug.WriteLine(this.FormatMessage(entry), entry.Level.ToString());
			}
		}
	}
}
