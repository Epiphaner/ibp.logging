﻿// ***********************************************************************
// Assembly         : Ibp.Logging
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="LoggerWriter.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging
{
	using System;
	using System.IO;
	using System.Text;
	using Ibp.Logging.Core;

	/// <summary>
	/// TextWriter wrapper for a <see cref="Logger" /> instance.
	/// </summary>
	/// <seealso cref="TextWriter" />
	public class LoggerWriter : TextWriter
	{
		private readonly LogLevel logLevel;
		private Logger logger;
		private bool disposed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="LoggerWriter"/> class.
		/// </summary>
		/// <param name="logger">The logger.</param>
		/// <param name="logLevel">The <see cref="LogLevel"/> to use for the logged messages.</param>
		public LoggerWriter(Logger logger, LogLevel logLevel)
		{
			this.logger = logger;
			this.logLevel = logLevel;
		}

		/// <summary>
		/// Gets returns the character encoding in which the output is written.
		/// </summary>
		public override Encoding Encoding => Encoding.Unicode;

		/// <summary>
		/// Gets or sets the line terminator string used by the current TextWriter.
		/// </summary>
		public override string NewLine { get => base.NewLine; set => base.NewLine = value; }

		/// <summary>
		/// Gets an object that controls formatting.
		/// </summary>
		public override IFormatProvider FormatProvider => base.FormatProvider;

		/// <summary>
		/// Clears all buffers for the current writer and causes any buffered data to be written to the underlying device.
		/// </summary>
		public override void Flush()
		{
			this.CheckDisposed();
			this.logger.WaitForLogToFinish();
		}

		/// <summary>
		/// Writes a character to the text string or stream.
		/// </summary>
		/// <param name="value">The character to write to the text stream.</param>
		public override void Write(char value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes a character array to the text string or stream.
		/// </summary>
		/// <param name="buffer">The character array to write to the text stream.</param>
		public override void Write(char[] buffer)
		{
			this.CheckDisposed();
			this.Write(new string(buffer));
		}

		/// <summary>
		/// Writes a subarray of characters to the text string or stream.
		/// </summary>
		/// <param name="buffer">The character array to write data from.</param>
		/// <param name="index">The character position in the buffer at which to start retrieving data.</param>
		/// <param name="count">The number of characters to write.</param>
		/// <exception cref="ArgumentNullException"><paramref name="buffer"/> is null. </exception>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> or <paramref name="count"/> is negative. </exception>
		/// <exception cref="ArgumentException">The buffer length minus index is less than count.</exception>
		public override void Write(char[] buffer, int index, int count)
		{
			this.CheckDisposed();

			if (buffer == null)
			{
				throw new ArgumentNullException(nameof(buffer));
			}

			if (index < 0)
			{
				throw new ArgumentOutOfRangeException($"{nameof(index)} is negative.");
			}

			if (count < 0)
			{
				throw new ArgumentOutOfRangeException($"{nameof(count)} is negative.");
			}

			if (buffer.Length - index < count)
			{
				throw new ArgumentException("The buffer length minus index is less than count.");
			}

			this.Write(new string(buffer, index, count));
		}

		/// <summary>
		/// Writes the text representation of a Boolean value to the text string or stream.
		/// </summary>
		/// <param name="value">The Boolean value to write.</param>
		public override void Write(bool value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a 4-byte signed integer to the text string or stream.
		/// </summary>
		/// <param name="value">The 4-byte signed integer to write.</param>
		public override void Write(int value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a 4-byte unsigned integer to the text string or stream.
		/// </summary>
		/// <param name="value">The 4-byte unsigned integer to write.</param>
		public override void Write(uint value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of an 8-byte signed integer to the text string or stream.
		/// </summary>
		/// <param name="value">The 8-byte signed integer to write.</param>
		public override void Write(long value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of an 8-byte unsigned integer to the text string or stream.
		/// </summary>
		/// <param name="value">The 8-byte unsigned integer to write.</param>
		public override void Write(ulong value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a 4-byte floating-point value to the text string or stream.
		/// </summary>
		/// <param name="value">The 4-byte floating-point value to write.</param>
		public override void Write(float value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of an 8-byte floating-point value to the text string or stream.
		/// </summary>
		/// <param name="value">The 8-byte floating-point value to write.</param>
		public override void Write(double value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a decimal value to the text string or stream.
		/// </summary>
		/// <param name="value">The decimal value to write.</param>
		public override void Write(decimal value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.Write(value.ToString(this.FormatProvider));
			}
			else
			{
				this.Write(value.ToString());
			}
		}

		/// <summary>
		/// Writes a string to the text string or stream.
		/// </summary>
		/// <param name="value">The string to write.</param>
		public override void Write(string value)
		{
			this.CheckDisposed();
			this.logger.Log(value, this.logLevel);
		}

		/// <summary>
		/// Writes the text representation of an object to the text string or stream by calling the ToString method on that object.
		/// </summary>
		/// <param name="value">The object to write.</param>
		public override void Write(object value)
		{
			this.CheckDisposed();
			this.Write(value.ToString());
		}

		/// <summary>
		/// Writes a formatted string to the text string or stream, using the same semantics as the <see cref="M:System.String.Format(System.String,System.Object)"></see> method.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg0">The object to format and write.</param>
		public override void Write(string format, object arg0)
		{
			this.CheckDisposed();
			this.Write(String.Format(format, arg0));
		}

		/// <summary>
		/// Writes a formatted string to the text string or stream, using the same semantics as the <see cref="M:System.String.Format(System.String,System.Object,System.Object)"></see> method.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg0">The first object to format and write.</param>
		/// <param name="arg1">The second object to format and write.</param>
		public override void Write(string format, object arg0, object arg1)
		{
			this.CheckDisposed();
			this.Write(String.Format(format, arg0, arg1));
		}

		/// <summary>
		/// Writes a formatted string to the text string or stream, using the same semantics as the <see cref="M:System.String.Format(System.String,System.Object,System.Object,System.Object)"></see> method.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg0">The first object to format and write.</param>
		/// <param name="arg1">The second object to format and write.</param>
		/// <param name="arg2">The third object to format and write.</param>
		public override void Write(string format, object arg0, object arg1, object arg2)
		{
			this.CheckDisposed();
			this.Write(String.Format(format, arg0, arg1, arg2));
		}

		/// <summary>
		/// Writes a formatted string to the text string or stream, using the same semantics as the <see cref="M:System.String.Format(System.String,System.Object[])"></see> method.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg">An object array that contains zero or more objects to format and write.</param>
		public override void Write(string format, params object[] arg)
		{
			this.CheckDisposed();
			this.Write(String.Format(format, arg));
		}

		/// <summary>
		/// Writes a line terminator to the text string or stream.
		/// </summary>
		public override void WriteLine()
		{
			this.CheckDisposed();
			this.Write(this.NewLine);
		}

		/// <summary>
		/// Writes a character followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The character to write to the text stream.</param>
		public override void WriteLine(char value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes an array of characters followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="buffer">The character array from which data is read.</param>
		public override void WriteLine(char[] buffer)
		{
			this.CheckDisposed();
			this.WriteLine(new string(buffer));
		}

		/// <summary>
		/// Writes a subarray of characters followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="buffer">The character array from which data is read.</param>
		/// <param name="index">The character position in buffer at which to start reading data.</param>
		/// <param name="count">The maximum number of characters to write.</param>
		/// <exception cref="ArgumentNullException"><paramref name="buffer"/> is null. </exception>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> or <paramref name="count"/> is negative. </exception>
		/// <exception cref="ArgumentException">The buffer length minus index is less than count.</exception>
		public override void WriteLine(char[] buffer, int index, int count)
		{
			this.CheckDisposed();

			if (buffer == null)
			{
				throw new ArgumentNullException(nameof(buffer));
			}

			if (index < 0)
			{
				throw new ArgumentOutOfRangeException($"{nameof(index)} is negative.");
			}

			if (count < 0)
			{
				throw new ArgumentOutOfRangeException($"{nameof(count)} is negative.");
			}

			if (buffer.Length - index < count)
			{
				throw new ArgumentException("The buffer length minus index is less than count.");
			}

			this.WriteLine(new string(buffer, index, count));
		}

		/// <summary>
		/// Writes the text representation of a Boolean value followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The Boolean value to write.</param>
		public override void WriteLine(bool value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a 4-byte signed integer followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The 4-byte signed integer to write.</param>
		public override void WriteLine(int value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a 4-byte unsigned integer followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The 4-byte unsigned integer to write.</param>
		public override void WriteLine(uint value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of an 8-byte signed integer followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The 8-byte signed integer to write.</param>
		public override void WriteLine(long value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of an 8-byte unsigned integer followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The 8-byte unsigned integer to write.</param>
		public override void WriteLine(ulong value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a 4-byte floating-point value followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The 4-byte floating-point value to write.</param>
		public override void WriteLine(float value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a 8-byte floating-point value followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The 8-byte floating-point value to write.</param>
		public override void WriteLine(double value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes the text representation of a decimal value followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The decimal value to write.</param>
		public override void WriteLine(decimal value)
		{
			this.CheckDisposed();
			if (this.FormatProvider != null)
			{
				this.WriteLine(value.ToString(this.FormatProvider));
			}
			else
			{
				this.WriteLine(value.ToString());
			}
		}

		/// <summary>
		/// Writes a string followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The string to write. If value is null, only the line terminator is written.</param>
		public override void WriteLine(string value)
		{
			this.CheckDisposed();
			this.Write(value + this.NewLine);
		}

		/// <summary>
		/// Writes the text representation of an object by calling the ToString method on that object, followed by a line terminator to the text string or stream.
		/// </summary>
		/// <param name="value">The object to write. If value is null, only the line terminator is written.</param>
		public override void WriteLine(object value)
		{
			this.CheckDisposed();
			this.WriteLine(value.ToString());
		}

		/// <summary>
		/// Writes a formatted string and a new line to the text string or stream, using the same semantics as the <see cref="M:System.String.Format(System.String,System.Object)"></see> method.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg0">The object to format and write.</param>
		public override void WriteLine(string format, object arg0)
		{
			this.CheckDisposed();
			this.WriteLine(String.Format(format, arg0));
		}

		/// <summary>
		/// Writes a formatted string and a new line to the text string or stream, using the same semantics as the <see cref="M:System.String.Format(System.String,System.Object,System.Object)"></see> method.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg0">The first object to format and write.</param>
		/// <param name="arg1">The second object to format and write.</param>
		public override void WriteLine(string format, object arg0, object arg1)
		{
			this.CheckDisposed();
			this.WriteLine(String.Format(format, arg0, arg1));
		}

		/// <summary>
		/// Writes out a formatted string and a new line, using the same semantics as <see cref="M:System.String.Format(System.String,System.Object)"></see>.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg0">The first object to format and write.</param>
		/// <param name="arg1">The second object to format and write.</param>
		/// <param name="arg2">The third object to format and write.</param>
		public override void WriteLine(string format, object arg0, object arg1, object arg2)
		{
			this.CheckDisposed();
			this.WriteLine(String.Format(format, arg0, arg1, arg2));
		}

		/// <summary>
		/// Writes out a formatted string and a new line, using the same semantics as <see cref="M:System.String.Format(System.String,System.Object)"></see>.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg">An object array that contains zero or more objects to format and write.</param>
		public override void WriteLine(string format, params object[] arg)
		{
			this.CheckDisposed();
			this.WriteLine(String.Format(format, arg));
		}

		/// <summary>
		/// Releases the unmanaged resources used by the <see cref="T:System.IO.TextWriter"></see> and optionally releases the managed resources.
		/// </summary>
		/// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			this.CheckDisposed();
			IDisposable disposableLogger = this.logger;
			disposableLogger.Dispose();
			this.logger = null;
			this.disposed = true;
			base.Dispose(disposing);
		}

		/// <summary>
		/// Checks if the object is disposed.
		/// </summary>
		/// <exception cref="ObjectDisposedException">Thrown when the object is disposed. </exception>
		private void CheckDisposed()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(null);
			}
		}
	}
}
