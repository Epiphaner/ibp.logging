﻿// ***********************************************************************
// Assembly         : Ibp.Logging
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="StaticLogger.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging {
	using System.Runtime.CompilerServices;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// Provides static access to <see cref="Logger"/> methods and options.
	/// Thread-safe.
	/// </summary>
	public static class StaticLogger {
		/// <summary>
		/// Gets or sets get or Set the configuration for the logger.
		/// </summary>
		public static LoggerConfiguration LogConfiguration { get => LogInstance.Configuration; set => LogInstance.Configuration = value; }

		/// <summary>
		/// Gets get the internal instance of <see cref="Logger"/>.
		/// </summary>
		public static Logger LogInstance { get; } = new Logger();

		/// <summary>
		/// Pauses the thread until the logger has finished writing the logs.
		/// </summary>
		public static void WaitForLogToFinish() => LogInstance.WaitForLogToFinish();

		/// <summary>
		/// Logs a message.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Logger.Configuration" />.</param>
		/// <param name="level">The <see cref="LogLevel" /> indicating the severity of this <paramref name="message" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void Log<T>(T message, LogLevel level, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.Log(message, level, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="level">The <see cref="LogLevel" /> indicating the severity of this <paramref name="message" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void Log(string message, LogLevel level, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.Log(message, level, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.ERROR" />.
		/// Use this for problems that should and affect normal operation.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Logger.Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogError<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogError(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.ERROR" />.
		/// Use this for problems that should and affect normal operation.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogError(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogError(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.WARNING" />.
		/// Use this for problems that should not occur but don't hamper normal operation.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Logger.Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogWarning<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogWarning(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.WARNING" />.
		/// Use this for problems that should not occur but don't hamper normal operation.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogWarning(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogWarning(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.INFO" />.
		/// Use this for happenings within expectations that are significant.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Logger.Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogInfo<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogInfo(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.INFO" />.
		/// Use this for happenings within expectations that are significant.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogInfo(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogInfo(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.DEBUG" />.
		/// Use this for logging the flow of the system.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Logger.Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogDebug<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogDebug(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.DEBUG" />.
		/// Use this for logging the flow of the system.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogDebug(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogDebug(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.TRACE" />.
		/// Use this for the most detailed messages.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Logger.Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogTrace<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogTrace(message, methodName, lineNumber, filePath);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.TRACE" />.
		/// Use this for the most detailed messages.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public static void LogTrace(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			LogInstance.LogTrace(message, methodName, lineNumber, filePath);
		}
	}
}
