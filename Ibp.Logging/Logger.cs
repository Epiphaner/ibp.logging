﻿// ***********************************************************************
// Assembly         : Ibp.Logging
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="Logger.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging
{
	using System;
	using System.Collections.Concurrent;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;
	using System.Runtime.CompilerServices;
	using System.Runtime.ExceptionServices;
	using System.Threading;
	using System.Threading.Tasks;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.Core.LogTargets;
	using Ibp.Logging.Core.Serialization;

	/// <summary>
	/// Performance optimized logger using an instance.
	/// By using instances, multiple loggers at the same time-share are possible.
	/// </summary>
	public class Logger : ILogger, IDisposable
	{
		/// <summary>
		/// The interval for the log processing timer.
		/// </summary>
		private readonly TimeSpan _timerInterval = TimeSpan.FromMilliseconds(50);

		/// <summary>
		/// Object for locking threads out of simultanous access.
		/// </summary>
		private object _locker;

		/// <summary>
		/// Queue for all log messages to be put in.
		/// </summary>
		private ConcurrentQueue<LogEntry> _queue;

		/// <summary>
		/// Queue for all string typed log messages to be put in.
		/// </summary>
		private ConcurrentQueue<StringPassingObject> _stringMessageQueue;

		/// <summary>
		/// Queue for all errors encountered during the execution of logger methods.
		/// </summary>
		private ConcurrentQueue<Exception> _exceptionQueue;

		/// <summary>
		/// The last batch of tasks that was run.
		/// </summary>
		private Task[] _lastLogBatch = new Task[0];

		/// <summary>
		/// Contains the configuration for this logging instance.
		/// </summary>
		private LoggerConfiguration _configuration = new LoggerConfiguration();

		/// <summary>
		/// Generates threads at regular intervals to write the contents of the queue to the targets.
		/// </summary>
		private Timer _timer;

		/// <summary>
		/// Wether or not the configuration has been checked.
		/// </summary>
		private bool _configurationChecked = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="Logger"/> class.
		/// </summary>
		public Logger()
		{
			// Make sure the logger starts disabled if no configuration has been provided.
			this.Configuration.Enabled = false;

			// And instantiate all fields that should be instantiated in a certain order.
			this.InstantiateFields();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Logger"/> class.
		/// Instantiates a logger using the provided configuration.
		/// </summary>
		/// <param name="configuration">The <see cref="LoggerConfiguration"/> to use. Will keep the reference, so be careful where else you manipulate the object. </param>
		public Logger(LoggerConfiguration configuration)
		{
			// Set the configuration.
			this.Configuration = configuration;

			// And instantiate all fields that should be instantiated in a certain order.
			this.InstantiateFields();
		}

		/// <summary>
		/// Finalizes an instance of the <see cref="Logger"/> class.
		/// </summary>
		~Logger()
		{
			if (this.Configuration.FlushBeforeFinalization)
			{
				this.WaitForLogToFinish();
			}
		}

		/// <summary>
		/// Gets or sets the configuration for this logger instance.
		/// </summary>
		public LoggerConfiguration Configuration {
			get => this._configuration;
			set => this._configuration = value ?? new LoggerConfiguration();
		}

		/// <summary>
		/// Pauses the thread until the logger has finished writing the logs.
		/// </summary>
		public void WaitForLogToFinish()
		{
			this.ProcessLog(true);
			if (this._lastLogBatch.Any())
			{
				Task.WaitAll(this._lastLogBatch);
			}
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <inheritdoc />
		public void Dispose()
		{
			this._timer.Dispose();
		}

		/// <summary>
		/// Logs a message.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="level">The <see cref="LogLevel" /> indicating the severity of this <paramref name="message" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void Log<T>(T message, LogLevel level, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
			=> this.EnqueueLog(message, level, filePath, methodName, lineNumber);

		/// <summary>
		/// Logs a message.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="level">The <see cref="LogLevel" /> indicating the severity of this <paramref name="message" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void Log(string message, LogLevel level, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
			=> this.EnqueueLog(message, level, filePath, methodName, lineNumber);

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.ERROR" />.
		/// Use this for problems that should and affect normal operation.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogError<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			this.EnqueueLog(message, LogLevel.ERROR, filePath, methodName, lineNumber);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.ERROR" />.
		/// Use this for problems that should and affect normal operation.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogError(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
			=> this.EnqueueLog(message, LogLevel.ERROR, filePath, methodName, lineNumber);

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.WARNING" />.
		/// Use this for problems that should not occur but don't hamper normal operation.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogWarning<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			this.EnqueueLog(message, LogLevel.WARNING, filePath, methodName, lineNumber);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.WARNING" />.
		/// Use this for problems that should not occur but don't hamper normal operation.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogWarning(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
					=> this.EnqueueLog(message, LogLevel.WARNING, filePath, methodName, lineNumber);

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.INFO" />.
		/// Use this for happenings within expectations that are significant.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogInfo<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			this.EnqueueLog(message, LogLevel.INFO, filePath, methodName, lineNumber);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.INFO" />.
		/// Use this for happenings within expectations that are significant.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogInfo(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			this.EnqueueLog(message, LogLevel.INFO, filePath, methodName, lineNumber);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.DEBUG" />.
		/// Use this for logging the flow of the system.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogDebug<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			this.EnqueueLog(message, LogLevel.DEBUG, filePath, methodName, lineNumber);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.DEBUG" />.
		/// Use this for logging the flow of the system.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogDebug(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			this.EnqueueLog(message, LogLevel.DEBUG, filePath, methodName, lineNumber);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.TRACE" />.
		/// Use this for the most detailed messages.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogTrace<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			this.EnqueueLog(message, LogLevel.TRACE, filePath, methodName, lineNumber);
		}

		/// <summary>
		/// Logs a message with a <see cref="LogLevel" /> of <see cref="LogLevel.TRACE" />.
		/// Use this for the most detailed messages.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		public void LogTrace(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			this.EnqueueLog(message, LogLevel.TRACE, filePath, methodName, lineNumber);
		}

		/// <summary>
		/// Checks the configuration for inconsistencies or potential problems.
		/// </summary>
		/// <returns>A list containing all messages regarding the configuration.</returns>
		public ConfigurationTestResults CheckConfiguration()
		{
			this._configurationChecked = true;
			var results = new ConfigurationTestResults();
			foreach (LogTargetBase item in this.Configuration.GetLogTargets())
			{
				item.TestConfiguration(results);
			}

			this.TestConfiguration(results);
			return results;
		}

		/// <summary>
		/// Starts this Logger.
		/// It is recommended <see cref="CheckConfiguration" /> is called and checked before calling this.
		/// </summary>
		/// <param name="logLoggerStarted">if set to <c>true</c> the logger will log an info entry that it was started.</param>
		public void Start(bool logLoggerStarted = true)
		{
			foreach (LogTargetBase logTarget in this.Configuration.GetLogTargets())
			{
				logTarget.InitializeTarget();
			}

			this.Configuration.Enabled = true;

			if (logLoggerStarted) {
				this.LogInfo("Logger started");
			}

			if (!this._configurationChecked)
			{
				this.LogWarning($"Configuration was not checked before starting the logger. Please call {nameof(Logger)}.{nameof(this.CheckConfiguration)} and check the results before calling {nameof(Logger)}.{nameof(this.Start)}.");
			}

			_ = this._timer.Change(this._timerInterval, this._timerInterval);
		}

		/// <summary>
		/// Stops this logger. Optionally flushes the log and waits for it to be fully processed.
		/// </summary>
		/// <param name="flushLog">if set to <c>true</c> flushes the log before returning.</param>
		public void Stop(bool flushLog = true)
		{
			_ = this._timer.Change(Timeout.Infinite, Timeout.Infinite);

			if (flushLog)
			{
				bool paused = this.Configuration.Paused;
				this.Configuration.Paused = false;
				this.WaitForLogToFinish();
				this.Configuration.Paused = paused;
			}

			this.Configuration.Enabled = false;

			foreach (LogTargetBase target in this.Configuration.GetLogTargets()) {
				target.FinalizeTarget();
			}
		}

		/// <summary>
		/// Adds an exception to be handled internally.
		/// </summary>
		/// <param name="exception">The <see cref="Exception"/> to add.</param>
		internal void AddException(Exception exception)
		{
			this._exceptionQueue.Enqueue(exception);
		}

		/// <summary>
		/// Test if the <see cref="LogLevel"/> is allowed according to the given configuration.
		/// </summary>
		/// <param name="level">The level to test.</param>
		/// <param name="configuration">The configuration to test with.</param>
		/// <returns><c>true</c> if the <paramref name="level"/> is allowed by the <paramref name="configuration"/>, <c>false</c> otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static bool LogLevelAllowed(LogLevel level, LogConfigurationBase configuration) => !configuration.IgnoredLogLevels.HasFlag(level);

		/// <summary>
		/// Tests the configuration for problems and inconsistencies.
		/// </summary>
		/// <param name="testResults">The test results.</param>
		private void TestConfiguration(ConfigurationTestResults testResults)
		{
			const string name = "Logger";
			List<LogTargetBase> logTargets = this.Configuration.GetLogTargets();

			// Check if any logtargets have been registered.
			if (logTargets.Count < 1)
			{
				testResults.AddMessage(name, LogLevel.ERROR, "No logtargets registered");
			}
			else
			{
				testResults.AddMessage(name, LogLevel.INFO, $"{logTargets.Count} logtarget(s) registered");
			}

			// Check if this logger is enabled.
			if (this.Configuration.Enabled)
			{
				testResults.AddMessage(name, LogLevel.WARNING, "The logger is enabled, please check the configuration before starting the logger");
			}
			else
			{
				testResults.AddMessage(name, LogLevel.INFO, "The logger is not enabled, call Start to enable the logger");
			}

			// Check if any logtargets have been disabled.
			foreach (LogTargetBase logTarget in logTargets)
			{
				if (logTarget.GetLogTargetConfiguration().Enabled)
				{
					testResults.AddMessage(name, LogLevel.INFO, $"The logtarget \"{logTarget.Name}\" is enabled");
				}
				else
				{
					testResults.AddMessage(name, LogLevel.INFO, $"The logtarget \"{logTarget.Name}\" is not enabled");
				}
			}

			// Check if we're logging on all levels.
			// The configuration on the logger should reflect the logging capacity of the targets.
			foreach (LogLevel level in Enum.GetValues(typeof(LogLevel)))
			{
				if (LogLevelAllowed(level, this.Configuration))
				{
					if (logTargets.Any((target) => LogLevelAllowed(level, target.GetLogTargetConfiguration())))
					{
						testResults.AddMessage(name, LogLevel.DEBUG, $"Log entries of level {level} are accepted by this logger and at least one logtarget");
					}
					else
					{
						testResults.AddMessage(name, LogLevel.WARNING, $"Log entries of level {level} are accepted by this logger but none of the logtargets");
					}
				}
				else
				{
					testResults.AddMessage(name, LogLevel.DEBUG, $"Log entries of level {level} are not accepted by this logger");
				}
			}

			// Test the exception handling.
			if (this.Configuration.LogTargetExceptionHandler is null)
			{
				testResults.AddMessage(name, LogLevel.WARNING, "No exception handler has been set");
			}
			else
			{
				testResults.AddMessage(name, LogLevel.DEBUG, "An exception handler has been set");
			}
		}

		/// <summary>
		/// Enqueues the log message.
		/// </summary>
		/// <param name="message">The message to log.</param>
		/// <param name="level">The level of severity.</param>
		/// <param name="callerPath">The path to the caller that spawned the message.</param>
		/// <param name="methodName">Name of the method that spawned the message.</param>
		/// <param name="lineNumber">The line number where the message was spawned.</param>
		private void EnqueueLog(string message, LogLevel level, string callerPath, string methodName, int lineNumber)
		{
			if (!this.Configuration.Enabled)
			{
				return;
			}

			this._stringMessageQueue.Enqueue(
				new StringPassingObject()
				{
					Message = message,
					Path = callerPath,
					Method = methodName,
					LineNumber = lineNumber,
					Level = level,
					Timestamp = DateTimeOffset.Now,
				});
		}

		/// <summary>
		/// Enqueues a clone of <paramref name="message"/> for future processing. Uses the value itself if it is an atomic value.
		/// </summary>
		private void EnqueueLog<T>(T message, LogLevel level, string callerPath, string methodName, int lineNumber)
		{
			if (!this.Configuration.Enabled)
			{
				return;
			}

			if (message is string stringMessage)
			{
				this._queue.Enqueue(LogEntry.Get(stringMessage, callerPath, methodName, lineNumber, level, DateTimeOffset.Now));
				return;
			}

			if (typeof(T).IsImmutable())
			{
				this._queue.Enqueue(LogEntry.Get(message.ToString(), callerPath, methodName, lineNumber, level, DateTimeOffset.Now));
				return;
			}

			if (message == null)
			{
				this._queue.Enqueue(LogEntry.Get(String.Empty, callerPath, methodName, lineNumber, level, DateTimeOffset.Now));
				return;
			}

			if (message is Exception exception)
			{
				this._queue.Enqueue(LogEntry.Get(exception, callerPath, methodName, lineNumber, level, DateTimeOffset.Now));
				return;
			}

			if (LogLevelAllowed(level, this.Configuration))
			{
				this._queue.Enqueue(LogEntry.Get(TypeReader.ConvertToFieldTree(message, "root", 10), callerPath, methodName, lineNumber, level, DateTimeOffset.Now));
			}
		}

		/// <summary>
		/// Tries to log any exceptions that happened.
		/// Throws the exceptions if a debugger is attached.
		/// </summary>
		private void ProcessExceptions()
		{
			int i = 0;
			while (this._exceptionQueue.TryDequeue(out Exception exception))
			{
				// Cap the amount of errors logged to prevent logging loops from escalating.
				// For example if trying to log an exception would throw two new exceptions.
				if (i++ < 10)
				{
					this.LogError(exception, exception.TargetSite.Name, -1, exception.Source);
				}

				if (Debugger.IsAttached && this.Configuration.ThrowExceptionsWhileDebugging)
				{
					ExceptionDispatchInfo.Capture(exception).Throw();
				}
			}
		}

		/// <summary>
		/// Instantiates the internal fields.
		/// </summary>
		private void InstantiateFields()
		{
			this._locker = new object();

			// Initialize the queue for messages.
			this._queue = new ConcurrentQueue<LogEntry>();
			this._stringMessageQueue = new ConcurrentQueue<StringPassingObject>();

			// Initialize the queue for exceptions.
			this._exceptionQueue = new ConcurrentQueue<Exception>();

			// Initialize the handler for bubbling up exceptions.
			if (this.Configuration.LogTargetExceptionHandler == null)
			{
				this.Configuration.LogTargetExceptionHandler = this.AddException;
			}

			// Initialize and start the scheduler for log writer threads.
			this._timer = new Timer(this.ProcessLog, false, Timeout.InfiniteTimeSpan, this._timerInterval);
		}

		/// <summary>
		/// Processes the log messages by sending them to the logtargets.
		/// </summary>
		/// <param name="stateInfo">The state information.</param>
		private void ProcessLog(object stateInfo)
		{
			// Do nothing if the logger is paused.
			if (this._configuration.Paused) { return; }

			// Get all the exceptions out of the way (while debugging).
			this.ProcessExceptions();

			// Only one thread at the time to prevent weaving shenanigans.
			lock (this._locker)
			{
				// Need to store the rows for batch processing when writing to files.
				var entries = new List<LogEntry>(this._queue.Count + this._stringMessageQueue.Count);

				// Get the log entry from the queue if there is one.
				while (this._queue.TryDequeue(out LogEntry entry))
				{
					if (LogLevelAllowed(entry.Level, this.Configuration))
					{
						entries.Add(entry);
					} else {
						entry.Release();
					}
				}

				// Then the string queue.
				while (this._stringMessageQueue.TryDequeue(out StringPassingObject entry))
				{
					if (LogLevelAllowed(entry.Level, this.Configuration))
					{
						// Do the conversion to LogEntry here to offload the log calling thread.
						entries.Add(LogEntry.Get(entry.Message, entry.Path, entry.Method, entry.LineNumber, entry.Level, entry.Timestamp));
					}
				}

				// The entries should be ordered in the order they came in.
				entries = entries.OrderBy(entry => entry.TimeStamp).ToList();

				// Only log if the logger is enabled.
				if (this._configuration.Enabled && entries.Count > 0)
				{
					// Keep track of all tasks we create.
					var tasks = new List<Task>();
					foreach (LogTargetBase target in this.Configuration.GetLogTargets())
					{
						// Run each target with a list of clones to make sure the logtargets do not interfere with eachother.
						var entriesForTarget = entries.AsParallel().AsOrdered().Select(entry => entry.Clone()).ToList();
						tasks.Add(Task.Run(() => target.LogInternal(entriesForTarget, (stateInfo as bool?) == true)));
					}

					// Release all logentries to the pool.
					// They are not used anymore.
					// Their clones will still be used by the logtargets.
					entries.ForEach(entry => entry.Release());

					// Assign this batch of tasks as the last batch that was run.
					this._lastLogBatch = tasks.ToArray();
				}
			}
		}

		/// <summary>
		/// Simple type for passing string messages from one place to the next.
		/// </summary>
		private struct StringPassingObject
		{
			/// <summary>
			/// Gets or sets the message.
			/// </summary>
			/// <value>The message.</value>
			public string Message { get; set; }

			/// <summary>
			/// Gets or sets the method that spawned the message.
			/// </summary>
			/// <value>The method.</value>
			public string Method { get; set; }

			/// <summary>
			/// Gets or sets the path of the file that spawned the message.
			/// </summary>
			/// <value>The path.</value>
			public string Path { get; set; }

			/// <summary>
			/// Gets or sets the line number that spawned the message.
			/// </summary>
			/// <value>The line.</value>
			public int LineNumber { get; set; }

			/// <summary>
			/// Gets or sets the severitylevel of the message.
			/// </summary>
			/// <value>The level.</value>
			public LogLevel Level { get; set; }

			/// <summary>
			/// Gets or sets the timestamp of the message.
			/// </summary>
			/// <value>The timestamp.</value>
			public DateTimeOffset Timestamp { get; set; }
		}
	}
}
