﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Event
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="EventLogTarget.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.LogTargets {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Runtime.InteropServices;

	using Ibp.Logging.Configuration;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.Core.LogTargets;

	/// <summary>
	/// LogTarget for logging to windows events.
	/// </summary>
	public class EventLogTarget : LogTarget<EventLogConfiguration> {
		private const string CustomFormatterWarning = @"A custom formatter has been defined for the EventLog target, but the EventLog target does not use formatting.
The formatter will be ignored. ";

		/// <summary>
		/// Initializes a new instance of the <see cref="EventLogTarget"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to use.</param>
		public EventLogTarget(EventLogConfiguration configuration)
			: base(configuration) {
			if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
				throw new PlatformNotSupportedException($"{nameof(EventLogTarget)} can only be used on Windows");
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EventLogTarget"/> class.
		/// Uses the default configuration.
		/// </summary>
		public EventLogTarget()
			: this(new EventLogConfiguration()) { }

		/// <inheritdoc/>
		public override void TestConfiguration(ConfigurationTestResults testResults)
		{
			if (String.IsNullOrWhiteSpace(this.Configuration.Source))
			{
				testResults.AddMessage(this, LogLevel.ERROR, "Source does not have a valid value");
			}

			if (String.IsNullOrWhiteSpace(this.Configuration.LogName))
			{
				testResults.AddMessage(this, LogLevel.ERROR, "LogName does not have a valid value");
			}
		}

		/// <inheritdoc/>
		public override void InitializeTarget()
		{
			// Create the log source if it does not exist.
			if (!EventLog.SourceExists(this.Configuration.Source))
			{
				EventLog.CreateEventSource(this.Configuration.Source, this.Configuration.LogName);
			}
		}

		/// <summary>
		/// Finalizes this instance.
		/// Finalization performs all one-off actions that should be performed after logging is done.
		/// </summary>
		public override void FinalizeTarget()
		{
			// No finalization needed.
		}

		/// <inheritdoc/>
		protected sealed override void Log(List<LogEntry> entries) {
			// Warn the user if they are doing something silly.
			if (this.Configuration.Format.IsCustomFormatDefined()) {
				entries.Add(LogEntry.Get(CustomFormatterWarning, "EventLogTarget", "Log", -1, LogLevel.WARNING, DateTime.Now));
			}

			// Create a reference to an EventLog.
			// The LogName is usually "Application".
			using (var eventLog = new EventLog(this.Configuration.LogName)) {
				// Set the source name too, usually the name of the application.
				eventLog.Source = this.Configuration.Source;

				// Loop through all the entries that made the cut.
				foreach (LogEntry entry in entries) {
					// Instead of fully formatting the message, we convert it to a string.
					// And we assign a valid EventLogEntryType that corresponds as closely as possible to the provided LogLevel.
					eventLog.WriteEntry(entry.GetMessage(this.GetSerializationType()), ConvertLoglevelToEventLogEntryType(entry.Level));
				}
			}
		}

		private static EventLogEntryType ConvertLoglevelToEventLogEntryType(LogLevel level) {
			switch (level) {
				case LogLevel.ERROR:
					return EventLogEntryType.Error;
				case LogLevel.WARNING:
					return EventLogEntryType.Warning;
				case LogLevel.INFO:
				case LogLevel.DEBUG:
				case LogLevel.TRACE:
					return EventLogEntryType.Information;
				default:
					throw new NotImplementedException($"No implementation yet for {nameof(LogLevel)} of type {level}");
			}
		}
	}
}
