﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Event
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="EventLogConfiguration.cs" company="ICT Business Partners">
// Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Configuration {
	using System.Reflection;
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// Contains a configuration regarding logging to the Windows event log.
	/// Implements the <see cref="LogConfigurationBase"/>.
	/// </summary>
	public class EventLogConfiguration : LogConfigurationBase {
		/// <summary>
		/// Gets or sets the source of the eventlog, usually the name of the application.
		/// </summary>
		public string Source { get; set; } = Assembly.GetExecutingAssembly().GetName().Name;

		/// <summary>
		/// Gets or sets the name of the log, usually "Application".
		/// </summary>
		public string LogName { get; set; } = "Application";
	}
}
