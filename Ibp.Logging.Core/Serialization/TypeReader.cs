﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 03-20-2019
// ***********************************************************************
// <copyright file="TypeReader.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Serialization {
	using System;
	using System.Collections.Generic;
	using System.Reflection;

	/// <summary>
	/// Enum for depicting the type of field based on the analysis of <see cref="TypeReader"/>.
	/// </summary>
	public enum FieldType {
		/// <summary>
		/// Immutable field.
		/// </summary>
		Value,

		/// <summary>
		/// Object field
		/// </summary>
		Object,

		/// <summary>
		/// Array or other enumerable field.
		/// </summary>
		Array,

		/// <summary>
		/// Field without value.
		/// </summary>
		Empty,

		/// <summary>
		/// Reference that has been visited before.
		/// </summary>
		AlreadyVisited,

		/// <summary>
		/// Field that was not read because the maximum depth has been reached.
		/// </summary>
		MaxDepth,
	}

	/// <summary>
	/// Class with methods for reading and analyzing objects.
	/// </summary>
	public static class TypeReader {
		/// <summary>
		/// Converts an object for use in the serializers.
		/// </summary>
		/// <param name="obj">The object to interpret. </param>
		/// <param name="name">The name to use for the object. </param>
		/// <returns>The object in the form of a tree of <see cref="FieldTreeNode"/>s. </returns>
		public static FieldTreeNode ConvertToFieldTree(object obj, string name) {
			return ConvertToFieldTree(obj, name, 10);
		}

		/// <summary>
		/// Converts an object for use in the serializers.
		/// </summary>
		/// <param name="obj">The object to interpret. </param>
		/// <param name="name">The name to use for the object. </param>
		/// <param name="maxDepth">The maximum amount of levels down the object will be explored.</param>
		/// <returns>The object in the form of a tree of <see cref="FieldTreeNode"/>s. </returns>
		public static FieldTreeNode ConvertToFieldTree(object obj, string name, int maxDepth)
		{
			var field = new FieldTreeNode { Name = name };
			GenerateFieldTree(field, obj, maxDepth);
			return field;
		}

		/// <summary>
		/// The method implements deep clone using reflection.
		/// </summary>
		/// <param name="root">A field representing the provided object. </param>
		/// <param name="obj">It is the object used to deep clone.</param>
		/// <param name="depth">The amount of times we may go deeper into the cloning. If zero, we don't want to clone anymore.</param>
		private static void GenerateFieldTree(FieldTreeNode root, object obj, int depth) {
			FieldTreeNode currentField = root;

			// Prepare the stack for making the previously recursive function iterative.
			var stack = new Stack<(FieldTreeNode f, object o, int d)>();
			stack.Push((currentField, obj, depth));

			// In this iterative function we can keep track of the objects we have already seen.
			var seenObjects = new Dictionary<object, FieldTreeNode>();

			while (stack.Count > 0) {
				// Get the parameters for the current iteration.
				(currentField, obj, depth) = stack.Pop();

				// Set the raw value for the current field.
				currentField.RawValue = obj;

				// Check if the field is null.
				if (obj == null) {
					currentField.Type = FieldType.Empty;
					continue;
				}

				// Check if we have reached the maximum depth.
				if (depth == 0) {
					currentField.Type = FieldType.MaxDepth;
					continue;
				}

				// Retrieve the type of the object we are working with and set the ValueType field in the metadata.
				Type type = obj.GetType();
				currentField.ValueType = type;

				// If the type of object is the value type, we will always get a new object when the original object is assigned to another variable.
				// So if the type of the object is primitive or enum, we just return the object.
				// We will process the struct type subsequently because the struct type may contain the reference fields.
				// If the string variables contain the same chars, they always refer to the same string in the heap.
				// So if the type of the object is string, we also return the object.
				// This is also why we do this before we check if the object has been previously seen.
				// We don't want a visual representation full of references to earlier fields just because the object contains some empty strings for example.
				if (type.IsImmutable()) {
					currentField.Type = FieldType.Value;
					continue;
				}

				// Check if this object has previously been seen in another field.
				if (seenObjects.TryGetValue(obj, out FieldTreeNode previousField)) {
					currentField.Type = FieldType.AlreadyVisited;
					currentField.PreviousLocation = previousField.GetLocation();
					continue;
				}

				// If it wasn't, add it to the dictionary.
				seenObjects[obj] = currentField;

				// We process recursively this method in the elements of the original array because the type of the element may be the reference type.
				if (type.IsArray) {
					currentField.Type = FieldType.Array;
					var array = obj as Array;
					for (int i = 0; i < array.Length; i++) {
						// Create a field for each element in the array.
						var field = new FieldTreeNode {
							// Assign the index
							Index = i,
							Parent = currentField,
						};
						currentField.ListValue.Add(field);

						// And put it on the stack for future evaluation.
						stack.Push((field, array.GetValue(i), depth - 1));
					}

					continue;
				}

				// If the type of the object is class or struct, it may contain the reference fields,
				// so we use reflection and process recursively this method in the fields of the object
				// to get the full object.
				// We use Type.IsValueType method here because there is no way to indicate directly whether
				// the Type is a struct type.
				if (type.IsClass || type.IsValueType) {
					currentField.Type = FieldType.Object;

					// Get all FieldInfo.
					IEnumerable<FieldInfo> fields = type.GetAllFields();

					foreach (FieldInfo fieldInfo in fields) {
						// Get the property associated with this field.
						PropertyInfo property = type.GetProperty(fieldInfo.Name);
						if (property != null) {
							// Don't assess this field if the associated property is virtual.
							if (property.GetGetMethod().IsVirtual) { continue; }
						}

						// Get the value of the field we are going to assess.
						object fieldValue = fieldInfo.GetValue(obj);

						// Format names in a userfriendly way when the field is a backingfield
						string fieldName = fieldInfo.Name;
						if (fieldName.StartsWith("<")) {
							fieldName = fieldName.Substring(1, fieldName.IndexOf('>') - 1);
						}

						// Create a Field object to store the metadata in.
						var field = new FieldTreeNode {
							Parent = currentField,
							Name = fieldName,
						};
						currentField.ListValue.Add(field);

						// And put it on the stack for future evaluation.
						stack.Push((field, fieldValue, depth - 1));
					}

					continue;
				}

				throw new ArgumentException("The object is unknown type");
			}
		}
	}
}
