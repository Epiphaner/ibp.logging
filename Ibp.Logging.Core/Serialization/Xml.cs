﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 03-20-2019
// ***********************************************************************
// <copyright file="Xml.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Serialization {
	using System;
	using System.Text;

	/// <summary>
	/// A XML-like "serializer".
	/// Output is meant to be read like XML but can deviate from the XML standard.
	/// Only meant as a way to convert an object into a human readable string.
	/// </summary>
	public static class Xml {
		private const string XMLStart = @"<?xml version=""1.0"" encoding=""UTF-8""?>";

		/// <summary>
		/// Convert an object into a string.
		/// </summary>
		/// <param name="obj">The object to convert. </param>
		/// <param name="name">The name to use for the object in the output. </param>
		/// <returns>A string version of the object. </returns>
		public static string Serialize(object obj, string name = "Root") {
			FieldTreeNode root = TypeReader.ConvertToFieldTree(obj, name);
			return $"{XMLStart}\n{ConvertToString(root, 0)}";
		}

		/// <summary>
		/// Convert a field tree into a string.
		/// </summary>
		/// <param name="fieldTreeNode">The field tree node to convert.</param>
		/// <returns>A string version of the tree.</returns>
		public static string Serialize(FieldTreeNode fieldTreeNode)
		{
			return $"{XMLStart}\n{ConvertToString(fieldTreeNode, 0)}";
		}

		private static string ConvertToString(FieldTreeNode field, int depth) {
			switch (field.Type) {
				case FieldType.Value:
					return AddTags(field.Name ?? field.ValueType.Name, Sanitize(field.RawValue.ToString()), depth, false);

				case FieldType.Object:
					// Create the brackets and print each field
					var objectSB = new StringBuilder();

					// Increase the indentation since we opened a bracket
					depth++;
					foreach (FieldTreeNode subField in field.ListValue) {
						// Print each field
						objectSB.Append(ConvertToString(subField, depth));
					}

					// Decrease the indentation before closing the bracket
					--depth;
					return AddTags(field.Name ?? field.ValueType.Name, objectSB.ToString(), depth, true);

				case FieldType.Array:
					// Create the brackets and print each field
					var arraySB = new StringBuilder();

					// Increase the indentation since we opened a bracket
					depth++;
					foreach (FieldTreeNode subField in field.ListValue) {
						// Print each field
						arraySB.Append(ConvertToString(subField, depth));
					}

					// Decrease the indentation before closing the bracket
					--depth;
					return AddTags(field.Name ?? field.ValueType.Name, arraySB.ToString(), depth, true);

				case FieldType.Empty:
					// No value to print, so just return null in string form
					return String.Empty;

				case FieldType.AlreadyVisited:
					// This reference field was already visited previously, so show the user where to find the values
					return AddTags(field.Name, Sanitize($"[This reference was already visited, see '{field.PreviousLocation}' for the values]"), depth, false);

				case FieldType.MaxDepth:
					// The maximum depth was reached at this point, so let the reader know
					return AddTags(field.Name, "[Maximum depth reached!]", depth, false);

				default:
					break;
			}

			throw new ArgumentOutOfRangeException(nameof(field.Type), "The given type was not yet implemented or is not supported. ");
		}

		private static string Tabs(int n) {
			return new string('\t', n);
		}

		private static string Sanitize(string value) {
			return value?.Replace("<", "&lt;").Replace(">", "&gt;");
		}

		private static string AddTags(string name, string value, int depth, bool newLine) {
			name = Sanitize(name);
			if (newLine) {
				return $"{Tabs(depth)}<{name}>\n{value}{Tabs(depth)}</{name}>\n";
			} else {
				return $"{Tabs(depth)}<{name}>{value}</{name}>\n";
			}
		}
	}
}
