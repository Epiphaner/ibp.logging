﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 03-20-2019
// ***********************************************************************
// <copyright file="FieldTreeNode.cs" company="ICT Business Partners">
//      Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Serialization {
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// Represents a field of an object analyzed by <see cref="TypeReader"/>.
	/// </summary>
	public class FieldTreeNode
	{
		/// <summary>
		/// Gets or sets the parent <see cref="FieldTreeNode"/> of this <see cref="FieldTreeNode"/>.
		/// </summary>
		public FieldTreeNode Parent { get; set; }

		/// <summary>
		/// Gets or sets the index of the value in this field if applicable.
		/// </summary>
		public int Index { get; set; }

		/// <summary>
		/// Gets or sets the name of the field in the containing type.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the <see cref="FieldType"/> given by the <see cref="TypeReader"/>.
		/// </summary>
		public FieldType Type { get; set; }

		/// <summary>
		/// Gets or sets the raw, unprocessed value of the field in the source.
		/// </summary>
		public object RawValue { get; set; }

		/// <summary>
		/// Gets or sets references to the analyzed children of this field.
		/// </summary>
		public List<FieldTreeNode> ListValue { get; set; } = new List<FieldTreeNode>();

		/// <summary>
		/// Gets or sets the <see cref="System.Type"/> of the value.
		/// </summary>
		public Type ValueType { get; set; }

		/// <summary>
		/// Gets or sets if the <see cref="Type"/> is <see cref="FieldType.AlreadyVisited"/>, this field contains a string representation of the location where the value was previously found.
		/// </summary>
		public string PreviousLocation { get; set; }

		/// <summary>
		/// Gets a path representing where this field can be found respective to the base analyzed object.
		/// </summary>
		/// <returns>A string representing the path from the root of the analyzed object to this field.</returns>
		public string GetLocation() {
			if (this.Parent == null) {
				if (String.IsNullOrWhiteSpace(this.Name)) {
					return "root";
				}

				return this.Name;
			}

			if (this.Parent.Type == FieldType.Array) {
				return $"{this.Parent.GetLocation()}[{this.Index}]";
			}

			return $"{this.Parent.GetLocation()}.{this.Name}";
		}
	}
}
