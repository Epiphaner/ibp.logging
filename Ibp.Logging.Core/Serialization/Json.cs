﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 03-20-2019
// ***********************************************************************
// <copyright file="Json.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Serialization {
	using System;
	using System.Globalization;
	using System.Linq;
	using System.Text;

	/// <summary>
	/// A Json-like "serializer".
	/// Output is meant to be read like Json but can deviate from the Json standard.
	/// Only meant as a way to convert an object into a human readable string.
	/// </summary>
	public static class Json {
		private static readonly CultureInfo Culture = CultureInfo.GetCultureInfo("en-US");

		/// <summary>
		/// Convert an object into a string.
		/// </summary>
		/// <param name="obj">The object to convert. </param>
		/// <param name="name">The name to use for the object in the output. </param>
		/// <returns>A string version of the object. </returns>
		public static string Serialize(object obj, string name = "Root") {
			FieldTreeNode root = TypeReader.ConvertToFieldTree(obj, name);
			return $@"""{name}"": {ConvertToString(root, 0)}";
		}

		/// <summary>
		/// Convert a field tree into a string.
		/// </summary>
		/// <param name="fieldTreeNode">The field tree node to convert.</param>
		/// <returns>A string version of the tree.</returns>
		public static string Serialize(FieldTreeNode fieldTreeNode)
		{
			return $@"""{fieldTreeNode.Name}"": {ConvertToString(fieldTreeNode, 0)}";
		}

		private static string ConvertToString(FieldTreeNode field, int depth) {
			switch (field.Type) {
				case FieldType.Value:
					// Just return the value as a string
					return ValueToString(field.RawValue);

				case FieldType.Object:
					// Create the brackets and print each field
					var objectSB = new StringBuilder();
					objectSB.AppendLine("{");

					// Increase the indentation since we opened a bracket
					depth++;
					foreach (FieldTreeNode subField in field.ListValue) {
						string endSymbol = field.ListValue.Last() == subField ? String.Empty : ",";

						// Print each field
						objectSB.AppendLine($@"{Tabs(depth)}""{subField.Name}"": {ConvertToString(subField, depth)}{endSymbol}");
					}

					// Decrease the indentation before closing the bracket
					objectSB.Append($"{Tabs(--depth)}}}");
					return objectSB.ToString();

				case FieldType.Array:
					// Create the brackets and print each field
					var arraySB = new StringBuilder();
					arraySB.AppendLine("[");

					// Increase the indentation since we opened a bracket
					depth++;
					foreach (FieldTreeNode subField in field.ListValue) {
						string endSymbol = field.ListValue.Last() == subField ? String.Empty : ",";

						// Print each field
						arraySB.AppendLine($"{Tabs(depth)}{ConvertToString(subField, depth)}{endSymbol}");
					}

					// Decrease the indentation before closing the bracket
					arraySB.Append($"{Tabs(--depth)}]");
					return arraySB.ToString();

				case FieldType.Empty:
					// No value to print, so just return null in string form
					return "null";

				case FieldType.AlreadyVisited:
					// This reference field was already visited previously, so show the user where to find the values
					return $@"""[This reference was already visited, see '{field.PreviousLocation}' for the values]""";

				case FieldType.MaxDepth:
					// The maximum depth was reached at this point, so let the reader know
					return @"""[Maximum depth reached!]""";

				default:
					break;
			}

			throw new ArgumentOutOfRangeException(nameof(field.Type), "The given type was not yet implemented or is not supported. ");
		}

		private static string Tabs(int n) {
			return new string('\t', n);
		}

		private static string ValueToString(object value) {
			Type type = value.GetType();
			if (type.IsEnum) { return $"\"{Enum.GetName(type, value)}\""; }

			switch (Type.GetTypeCode(type)) {
				case TypeCode.Byte:
					return ((byte)value).ToString(Culture);
				case TypeCode.SByte:
					return ((sbyte)value).ToString(Culture);
				case TypeCode.UInt16:
					return ((ushort)value).ToString(Culture);
				case TypeCode.UInt32:
					return ((uint)value).ToString(Culture);
				case TypeCode.UInt64:
					return ((ulong)value).ToString(Culture);
				case TypeCode.Int16:
					return ((short)value).ToString(Culture);
				case TypeCode.Int32:
					return ((int)value).ToString(Culture);
				case TypeCode.Int64:
					return ((long)value).ToString(Culture);
				case TypeCode.Decimal:
					return ((decimal)value).ToString(Culture);
				case TypeCode.Double:
					return ((double)value).ToString(Culture);
				case TypeCode.Single:
					return ((float)value).ToString(Culture);
				default:
					return value == null ? "null" : $@"""{value.ToString().Replace(@"\", @"\\").Replace(@"""", @"\""")}""";
			}
		}
	}
}
