﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="LogEntry.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core
{
	using System;
	using System.Collections.Concurrent;
	using System.Collections.Generic;
	using System.Text;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.Core.Serialization;

	/// <summary>
	/// Represents an entry to be logged.
	/// </summary>
	public sealed class LogEntry
	{
		private const int MAXPOOLSIZE = 1000;
		private static readonly ConcurrentBag<LogEntry> Pool = new ConcurrentBag<LogEntry>();
		private FieldTreeNode objectToLog;
		private Exception exceptionToLog;
		private string messageToLog;

		private LogEntry()
		{
		}

		/// <summary>
		/// Gets the path to the file that called the logging method.
		/// </summary>
		public string CallerPath { get; private set; }

		/// <summary>
		/// Gets the name of the method in which the logging method was called.
		/// </summary>
		public string MethodName { get; private set; }

		/// <summary>
		/// Gets the line number where the logging method was called.
		/// </summary>
		public int LineNumber { get; private set; }

		/// <summary>
		/// Gets the <see cref="LogLevel"/> of the entry.
		/// </summary>
		public LogLevel Level { get; private set; }

		/// <summary>
		/// Gets the time at which the logging method was called.
		/// </summary>
		public DateTimeOffset TimeStamp { get; private set; }

		/// <summary>
		/// Gets a <see cref="LogEntry"/> from the pool or creates a new one.
		/// </summary>
		/// <param name="objectToLog">The object that should be logged. Can be a string.</param>
		/// <param name="callerPath">The path to the file that called the logging method.</param>
		/// <param name="methodName">The name of the method in which the logging method was called.</param>
		/// <param name="lineNumber">The line number where the logging method was called.</param>
		/// <param name="level">The <see cref="LogLevel"/> of the entry.</param>
		/// <param name="timeStamp">The time at which the logging method was called.</param>
		/// <returns>An instance of <see cref="LogEntry"/> with the provided values.</returns>
		public static LogEntry Get(FieldTreeNode objectToLog, string callerPath, string methodName, int lineNumber, LogLevel level, DateTimeOffset timeStamp)
		{
			return Get(
			  objectToLog: objectToLog,
			  exceptionToLog: null,
			  messageToLog: null,
			  callerPath: callerPath,
			  methodName: methodName,
			  lineNumber: lineNumber,
			  level: level,
			  timeStamp: timeStamp);
		}

		/// <summary>
		/// Gets a <see cref="LogEntry"/> from the pool or creates a new one.
		/// </summary>
		/// <param name="messageToLog">The message that should be logged.</param>
		/// <param name="callerPath">The path to the file that called the logging method.</param>
		/// <param name="methodName">The name of the method in which the logging method was called.</param>
		/// <param name="lineNumber">The line number where the logging method was called.</param>
		/// <param name="level">The <see cref="LogLevel"/> of the entry.</param>
		/// <param name="timeStamp">The time at which the logging method was called.</param>
		/// <returns>An instance of <see cref="LogEntry"/> with the provided values.</returns>
		public static LogEntry Get(string messageToLog, string callerPath, string methodName, int lineNumber, LogLevel level, DateTimeOffset timeStamp)
		{
			return Get(
			  objectToLog: null,
			  exceptionToLog: null,
			  messageToLog: messageToLog,
			  callerPath: callerPath,
			  methodName: methodName,
			  lineNumber: lineNumber,
			  level: level,
			  timeStamp: timeStamp);
		}

		/// <summary>
		/// Gets a <see cref="LogEntry"/> from the pool or creates a new one.
		/// </summary>
		/// <param name="exceptionToLog">The message that should be logged.</param>
		/// <param name="callerPath">The path to the file that called the logging method.</param>
		/// <param name="methodName">The name of the method in which the logging method was called.</param>
		/// <param name="lineNumber">The line number where the logging method was called.</param>
		/// <param name="level">The <see cref="LogLevel"/> of the entry.</param>
		/// <param name="timeStamp">The time at which the logging method was called.</param>
		/// <returns>An instance of <see cref="LogEntry"/> with the provided values.</returns>
		public static LogEntry Get(Exception exceptionToLog, string callerPath, string methodName, int lineNumber, LogLevel level, DateTimeOffset timeStamp)
		{
			return Get(
			  objectToLog: null,
			  exceptionToLog: exceptionToLog,
			  messageToLog: null,
			  callerPath: callerPath,
			  methodName: methodName,
			  lineNumber: lineNumber,
			  level: level,
			  timeStamp: timeStamp);
		}

		/// <summary>
		/// Get the message from this entry.
		/// </summary>
		/// <param name="serializationType">Type of the serialization to use if the message needs to be converted from an object.</param>
		/// <returns>The message to log.</returns>
		public string GetMessage(SerializationType serializationType)
		{
			if (this.messageToLog != null)
			{
				return this.messageToLog;
			}
			else if (this.objectToLog != null)
			{
				this.messageToLog = ObjectToString(this.objectToLog, serializationType);
			}
			else if (this.exceptionToLog != null)
			{
				// Alternative: this.messageToLog = this.exceptionToLog.GetBaseException().ToString();
				this.messageToLog = ConstructExceptionMessage(this.exceptionToLog);
			}

			return this.messageToLog;
		}

		/// <summary>
		/// Deconstructor for converting <see cref="LogEntry"/> instances into <see cref="ValueTuple"/>'s.
		/// </summary>
		/// <param name="callerPath">The path to the file that called the logging method.</param>
		/// <param name="methodName">The name of the method in which the logging method was called.</param>
		/// <param name="lineNumber">The line number where the logging method was called.</param>
		/// <param name="level">The <see cref="LogLevel"/> of the entry.</param>
		/// <param name="timeStamp">The time at which the logging method was called.</param>
		public void Deconstruct(out string callerPath, out string methodName, out int lineNumber, out LogLevel level, out DateTimeOffset timeStamp)
		{
			callerPath = this.CallerPath;
			methodName = this.MethodName;
			lineNumber = this.LineNumber;
			level = this.Level;
			timeStamp = this.TimeStamp;
		}

		/// <summary>
		/// Releases the <see cref="LogEntry"/> to the pool.
		/// </summary>
		public void Release()
		{
			if (Pool.Count < MAXPOOLSIZE)
			{
				Pool.Add(this);
			}
		}

		/// <summary>
		/// Clones the <see cref="LogEntry"/> instance into a new instance.
		/// </summary>
		/// <returns>A functionally identical <see cref="LogEntry"/>.</returns>
		public LogEntry Clone() => Get(this.objectToLog, this.exceptionToLog, this.messageToLog, this.CallerPath, this.MethodName, this.LineNumber, this.Level, this.TimeStamp);

		/// <summary>
		/// Construct a message for an <see cref="Exception"/>.
		/// </summary>
		/// <param name="exception">The <see cref="Exception"/> to construct the message for.</param>
		/// <returns>The message describing the <see cref="Exception"/>.</returns>
		private static string ConstructExceptionMessage(Exception exception)
		{
			const string innerExceptionsText = "Inner exceptions:";
			var builder = new StringBuilder();
			var exceptions = new Stack<(Exception, int)>();

			// Begin the process by adding the provided Exception.
			exceptions.Push((exception, 0));

			// Append an indented line to the StringBuilder.
			void AppendIndentedLine(int indents)
			{
				builder.AppendLine();
				for (int i = 0; i < indents; i++) {
					builder.Append('\t');
				}
			}

			while (exceptions.Count > 0) {
				(Exception currentException, int indents) = exceptions.Pop();
				AppendIndentedLine(indents);

				builder.Append(currentException);
				if (currentException is AggregateException aggregateException) {
					AppendIndentedLine(indents);
					builder.Append(innerExceptionsText);

					// Append each inner exception of an aggregate exception.
					foreach (Exception innerException in aggregateException.InnerExceptions) {
						exceptions.Push((innerException, indents + 1));
					}
				} else if (currentException.InnerException != null) {
					AppendIndentedLine(indents);
					builder.Append(innerExceptionsText);

					// If there is an inner exception, log it.
					exceptions.Push((currentException.InnerException, indents + 1));
				}
			}

			// Complete the message.
			return builder.ToString();
		}

		private static LogEntry Get(FieldTreeNode objectToLog, Exception exceptionToLog, string messageToLog, string callerPath, string methodName, int lineNumber, LogLevel level, DateTimeOffset timeStamp)
		{
			if (!Pool.TryTake(out LogEntry entry))
			{
				entry = new LogEntry();
			}

			entry.objectToLog = objectToLog;
			entry.exceptionToLog = exceptionToLog;
			entry.messageToLog = messageToLog;
			entry.CallerPath = callerPath;
			entry.MethodName = methodName;
			entry.LineNumber = lineNumber;
			entry.Level = level;
			entry.TimeStamp = timeStamp;

			return entry;
		}

		/// <summary>
		/// Converts the <paramref name="fieldTreeNode"/> <see cref="Object"/> into an as readable possible <see cref="String"/>.
		/// </summary>
		/// <param name="fieldTreeNode">The <see cref="Object"/> to be converted.</param>
		/// <param name="serializationType">The type of serialization to use.</param>
		/// <returns>The generated string.</returns>
		private static string ObjectToString(FieldTreeNode fieldTreeNode, SerializationType serializationType)
		{
			// If we are not going to serialize anyways, just convert the source to a string in the old-fashioned way.
			if (serializationType == SerializationType.NONE) { return fieldTreeNode.RawValue?.ToString() ?? String.Empty; }

			// The same goes for any numeric or null values.
			switch (Type.GetTypeCode(fieldTreeNode.ValueType))
			{
				case TypeCode.Object:
					break;
				case TypeCode.Empty:
					return "Null";
				case TypeCode.DBNull:
					return "DBNull";
				default:
					return fieldTreeNode.RawValue.ToString();
			}

			// Finally, if no other options remain, we serialize the object.
			switch (serializationType)
			{
				case SerializationType.JSON:
					return $"\n{Json.Serialize(fieldTreeNode)}";
				case SerializationType.XML:
					return $"\n{Xml.Serialize(fieldTreeNode)}";
				default:
					throw new NotImplementedException($"Invalid serialization type encountered: {serializationType}");
			}
		}
	}
}
