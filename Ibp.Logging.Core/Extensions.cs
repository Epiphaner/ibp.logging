﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 03-20-2019
// ***********************************************************************
// <copyright file="Extensions.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using System.Text;

	/// <summary>
	/// Contains extensions for global use.
	/// </summary>
	public static class Extensions
	{
		/// <summary>
		/// Checks if an instance of <paramref name="type"/> can be copied without fear of references carrying over.
		/// </summary>
		/// <param name="type">The type to check. </param>
		/// <returns>True if an instance is immutable. </returns>
		public static bool IsImmutable(this Type type) => type.IsPrimitive || type.IsEnum || type == typeof(string) || type == typeof(DateTime) || type == typeof(TimeSpan);

		/// <summary>
		/// Gets all fields of the type and its basetypes.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns>Collection with all fields.</returns>
		public static IEnumerable<FieldInfo> GetAllFields(this Type type) {
			if (type == null) {
				return Enumerable.Empty<FieldInfo>();
			}

			BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic |
						 BindingFlags.Static | BindingFlags.Instance |
						 BindingFlags.DeclaredOnly;
			return type.GetFields(flags).Concat(GetAllFields(type.BaseType));
		}
	}
}
