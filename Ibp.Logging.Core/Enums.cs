﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="Enums.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core {
	using System;

	/// <summary>
	/// The severity of a log entry.
	/// A higher level means less detail.
	/// So <see cref="INFO"/> is a higher <see cref="LogLevel"/> than <see cref="DEBUG"/>.
	/// </summary>
	[Flags]
	public enum LogLevel {
		/// <summary>
		/// The requested action cannot be completed.
		/// </summary>
		ERROR = 0b10000,

		/// <summary>
		/// The requested action can be completed, but there was something unexpected during execution.
		/// </summary>
		WARNING = 0b01000,

		/// <summary>
		/// Interesting runtime events.
		/// </summary>
		INFO = 0b00100,

		/// <summary>
		/// Superficial information about the flow of the process.
		/// </summary>
		DEBUG = 0b00010,

		/// <summary>
		/// Fully detailed information about all happenings within the process.
		/// </summary>
		TRACE = 0b00001,
	}
}
