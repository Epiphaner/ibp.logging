﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : RZaal
// Created          : 05-07-2019
//
// Last Modified By : RZaal
// Last Modified On : 05-07-2019
// ***********************************************************************
// <copyright file="ConfigurationMessage.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core {
	/// <summary>
	/// Contains all information regarding a message that can be spawned by a Logger or LogTarget.
	/// </summary>
	public class ConfigurationMessage {
		/// <summary>
		/// Initializes a new instance of the <see cref="ConfigurationMessage"/> class.
		/// </summary>
		/// <param name="moduleName">Name of the module that spawned the message.</param>
		/// <param name="level">The severity level of the message.</param>
		/// <param name="message">The message.</param>
		public ConfigurationMessage(string moduleName, LogLevel level, string message) {
			this.ModuleName = moduleName;
			this.Level = level;
			this.Message = message;
		}

		/// <summary>
		/// Gets the level of the message expressed as <see cref="LogLevel"/>.
		/// </summary>
		/// <value>The message level.</value>
		public LogLevel Level { get; }

		/// <summary>
		/// Gets the name of the module from which the message stems.
		/// </summary>
		/// <value>The module name.</value>
		public string ModuleName { get; }

		/// <summary>
		/// Gets the message itself.
		/// </summary>
		/// <value>The message.</value>
		public string Message { get; }
	}
}
