﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="FormatLogConfiguration.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Configuration {
	using System;

	/// <summary>
	/// Contains a configuration regarding the formatting of log messages.
	/// </summary>
	public class FormatLogConfiguration {
		/// <summary>
		/// Delegate for formatting the information into a nice looking string.
		/// </summary>
		/// <param name="message">The message to log. </param>
		/// <param name="level">The <see cref="LogLevel"/> of the message. </param>
		/// <param name="executionPath">Pre-formatted information about the line the message comes from. </param>
		/// <param name="timeStamp">The time at which the message was sent to the logger. </param>
		/// <returns>A formatted string. </returns>
		public delegate string SimpleLogFormatHandler(string message, LogLevel level, string executionPath, DateTimeOffset timeStamp);

		/// <summary>
		/// Delegate for formatting the information into a nice looking string.
		/// </summary>
		/// <param name="message">The message to log. </param>
		/// <param name="level">The <see cref="LogLevel"/> of the message. </param>
		/// <param name="callerPath">The path of the file from which the log action was called. </param>
		/// <param name="methodName">The name of the method from which the log action was called. </param>
		/// <param name="lineNumber">The line number on which the log action was called. </param>
		/// <param name="timeStamp">The time at which the message was sent to the logger. </param>
		/// <returns>A formatted string. </returns>
		public delegate string LogFormatHandler(string message, LogLevel level, string callerPath, string methodName, int lineNumber, DateTimeOffset timeStamp);

		/// <summary>
		/// Gets or sets you can assign a custom formatter if you don't want to use the built-in one.
		/// The execution path has been pre-formatted in the simple formatter.
		/// </summary>
		public SimpleLogFormatHandler SimpleFormatter { get; set; }

		/// <summary>
		/// Gets or sets you can assign a custom formatter if you don't want to use the built-in one.
		/// </summary>
		public LogFormatHandler Formatter { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether if true, the default formatter will show a timestamp for every log message.
		/// </summary>
		public bool ShowTimestamp { get; set; } = true;

		/// <summary>
		/// Gets or sets defines what type of serialization should be used when logging objects.
		/// </summary>
		public SerializationType? SerializationType { get; set; }

		/// <summary>
		/// Gets or sets when using serialization, how deep should the object be serialized?
		/// Higher values can have an impact on performance.
		/// </summary>
		public int SerializationDepth { get; set; } = 10;

		/// <summary>
		/// Check if a custom format has been defined.
		/// </summary>
		/// <returns>True if a custom format has been defined, false otherwise. </returns>
		public bool IsCustomFormatDefined() => this.SimpleFormatter != null || this.Formatter != null;
	}
}
