﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 03-20-2019
// ***********************************************************************
// <copyright file="Enums.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Configuration {
	/// <summary>
	/// The different types of available serialization.
	/// </summary>
	public enum SerializationType {
		/// <summary>
		/// Uses a subset of JavaScript syntax to represent objects.
		/// </summary>
		JSON,

		/// <summary>
		/// A very strict textual representation of objects.
		/// </summary>
		XML,

		/// <summary>
		/// No serialization, just prints a string with the name of the type instead.
		/// </summary>
		NONE,
	}
}
