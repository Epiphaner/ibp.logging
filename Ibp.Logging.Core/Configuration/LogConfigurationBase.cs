﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="LogConfigurationBase.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Configuration
{
	using System;

	/// <summary>
	/// These configuration should be defined on a general level and allowed to be further specified per logtarget.
	/// </summary>
	public abstract class LogConfigurationBase
	{
		private FormatLogConfiguration _format = new FormatLogConfiguration();
		private bool _enabled = true;
		private TimeSpan? _logUpdateInterval;

		/// <summary>
		/// Initializes a new instance of the <see cref="LogConfigurationBase"/> class.
		/// </summary>
		public LogConfigurationBase() { }

		/// <summary>
		/// Fires when <see cref="Enabled"/> was written to.
		/// </summary>
		public event EventHandler EnabledChanged;

		/// <summary>
		/// Gets or sets the configuration that defines the way rows are formatted.
		/// When you assign null to this field, an empty <see cref="FormatLogConfiguration"/> will be assigned instead.
		/// This Allows for easy resetting of the values without having a reference to <see cref="Configuration"/>.
		/// </summary>
		public FormatLogConfiguration Format { get => this._format; set => this._format = value ?? new FormatLogConfiguration(); }

		/// <summary>
		/// Gets or sets a value indicating whether if true, logs will be written to this target.
		/// </summary>
		public bool Enabled {
			get => this._enabled; set {
				bool changed = this._enabled != value;
				this._enabled = value;
				if (changed)
				{
					this.EnabledChanged?.Invoke(this, null);
				}
			}
		}

		/// <summary>
		/// Gets or sets what loglevels are ignored by this component.
		/// </summary>
		public LogLevel IgnoredLogLevels { get; set; }

		/// <summary>
		/// Gets or sets the time between each update of the log output.
		/// Warning: very low values can lead to instability.
		/// Default value: 100 milliseconds.
		/// Minimum value: 50 milliseconds.
		/// Only null and values of 50 milliseconds or higher are allowed.
		/// </summary>
		/// <exception cref="ArgumentOutOfRangeException">Thrown when the value is not null and smaller than 50 milliseconds.</exception>
		public TimeSpan? LogUpdateInterval {
			get => this._logUpdateInterval;

			set {
				if (value == null)
				{
					this._logUpdateInterval = value;
					return;
				}

				if (value < TimeSpan.FromMilliseconds(50))
				{
					throw new ArgumentOutOfRangeException(nameof(value), "The value is not 50 milliseconds or higher");
				}

				this._logUpdateInterval = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum log entries in cache.
		/// </summary>
		/// <value>The maximum log entries in cache.</value>
		public int? MaximumLogEntriesInCache { get; set; }
	}
}
