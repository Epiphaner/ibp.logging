﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="LoggerConfiguration.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Configuration {
	using System;
	using System.Collections.Generic;
	using Ibp.Logging.Core.LogTargets;

	/// <summary>
	/// The configuration for a logger.
	/// </summary>
	public sealed class LoggerConfiguration : LogConfigurationBase {
		/// <summary>
		/// Initializes a new instance of the <see cref="LoggerConfiguration"/> class.
		/// All logtargets need to be enabled seperately.
		/// By default, the <see cref="SerializationType"/> will be set to <see cref="SerializationType.JSON"/>.
		/// </summary>
		public LoggerConfiguration() {
			this.Enabled = true;
			this.Format.SerializationType = SerializationType.JSON;
			this.LogUpdateInterval = this.DefaultLogInterval;
		}

		/// <summary>
		/// Gets or sets a value indicating whether the logger should keep the processing of messages on hold until unpaused.
		/// </summary>
		public bool Paused { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the logger should throw any encountered exceptions when a debugger is attached.
		/// </summary>
		public bool ThrowExceptionsWhileDebugging { get; set; } = true;

		/// <summary>
		/// Gets or sets a value indicating whether all logs should be flushed to their targets when the logger is stopped.
		/// </summary>
		public bool FlushBeforeFinalization { get; set; } = true;

		/// <summary>
		/// Gets or sets the log target exception handler.
		/// </summary>
		/// <value>The log target exception handler.</value>
		public Action<Exception> LogTargetExceptionHandler { get; set; }

		/// <summary>
		/// Gets or sets default interval for processing log entries.
		/// </summary>
		internal TimeSpan DefaultLogInterval { get; set; } = TimeSpan.FromMilliseconds(100);

		/// <summary>
		/// Gets or sets the default maximum log entries in cache.
		/// </summary>
		/// <value>The default maximum log entries in cache.</value>
		internal int DefaultMaximumLogEntriesInCache { get; set; } = 10_000;

		/// <summary>
		/// Gets a list of all registered <see cref="LogTargetBase"/>s.
		/// </summary>
		internal List<LogTargetBase> LogTargets { get; } = new List<LogTargetBase>();

		/// <summary>
		/// Deregisters a <see cref="LogTargetBase"/> from this <see cref="LoggerConfiguration"/> instance.
		/// </summary>
		/// <param name="target">The <see cref="LogTargetBase"/> to deregister.</param>
		public void DeregisterLogTarget(LogTargetBase target) {
			this.LogTargets.Remove(target);
		}

		/// <summary>
		/// Gets a list of all logtargets associated with this logger.
		/// </summary>
		/// <returns>List of logtargets.</returns>
		public List<LogTargetBase> GetLogTargets() {
			return new List<LogTargetBase>(this.LogTargets);
		}

		/// <summary>
		/// Registers a <see cref="LogTargetBase"/> with this <see cref="LoggerConfiguration"/> instance.
		/// </summary>
		/// <param name="target">The <see cref="LogTargetBase"/> to register.</param>
		/// <typeparam name="TLogTarget">The type of logtarget to use.</typeparam>
		/// <returns>The registered target.</returns>
		public TLogTarget RegisterLogTarget<TLogTarget>(TLogTarget target)
			where TLogTarget : LogTargetBase {
			if (!this.LogTargets.Contains(target)) {
				target.LoggerConfiguration = this;
				this.LogTargets.Add(target);
			}

			return target;
		}
	}
}
