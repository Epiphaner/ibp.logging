﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="ConfigurationTestResults.cs" company="ICT Business Partners">
// Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.Configuration {
	using System.Collections.Generic;
	using Ibp.Logging.Core.LogTargets;

	/// <summary>
	/// Contains the results of a configuration test.
	/// </summary>
	public class ConfigurationTestResults {
		private readonly List<ConfigurationMessage> _messages = new List<ConfigurationMessage>();

		/// <summary>
		/// Adds a message to the testresults.
		/// </summary>
		/// <param name="logTarget">The logging target that generated the message.</param>
		/// <param name="level">The level of severity of the message.</param>
		/// <param name="message">The message.</param>
		public void AddMessage(LogTargetBase logTarget, LogLevel level, string message) {
			this._messages.Add(new ConfigurationMessage(logTarget?.Name, level, message));
		}

		/// <summary>
		/// Adds a message to the testresults.
		/// </summary>
		/// <param name="moduleName">The logging module that generated the message.</param>
		/// <param name="level">The level of severity of the message.</param>
		/// <param name="message">The message.</param>
		public void AddMessage(string moduleName, LogLevel level, string message) {
			this._messages.Add(new ConfigurationMessage(moduleName, level, message));
		}

		/// <summary>
		/// Gets all the messages.
		/// </summary>
		/// <returns>A <see cref="List{T}"/> with all messages added to this instance.</returns>
		public List<ConfigurationMessage> GetMessages() {
			return new List<ConfigurationMessage>(this._messages);
		}

		/// <summary>
		/// Gets all the messages from a given module.
		/// </summary>
		/// <param name="module">The module for which to retrieve the messages.</param>
		/// <returns>A <see cref="List{T}" /> with all messages added to this instance for the given module.</returns>
		public List<ConfigurationMessage> GetMessages(string module) {
			return this._messages.FindAll(new System.Predicate<ConfigurationMessage>((t) => t.ModuleName == module));
		}

		/// <summary>
		/// Gets all the messages from a given module.
		/// </summary>
		/// <param name="module">The module for which to retrieve the messages.</param>
		/// <returns>A <see cref="List{T}" /> with all messages added to this instance for the given module.</returns>
		public List<ConfigurationMessage> GetMessages(LogTargetBase module) {
			return this._messages.FindAll(new System.Predicate<ConfigurationMessage>((t) => t.ModuleName == module?.Name));
		}
	}
}