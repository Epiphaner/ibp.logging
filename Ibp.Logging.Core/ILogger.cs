﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 05-01-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="ILogger.cs" company="ICT Business Partners">
// Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Ibp.Logging.Core
{
	using System;
	using System.Collections.Generic;
	using System.Runtime.CompilerServices;
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// Interface for loggers based on the Ibp.Logging library.
	/// </summary>
	public interface ILogger
	{
		/// <summary>
		/// Gets or sets the configuration.
		/// </summary>
		/// <value>The configuration.</value>
		LoggerConfiguration Configuration { get; set; }

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.ERROR"/>.
		/// Use this for problems that should and affect normal operation.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogError<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.ERROR"/>.
		/// Use this for problems that should and affect normal operation.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogError(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.WARNING"/>.
		/// Use this for problems that should not occur but don't hamper normal operation.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogWarning<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.WARNING"/>.
		/// Use this for problems that should not occur but don't hamper normal operation.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogWarning(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.INFO"/>.
		/// Use this for happenings within expectations that are significant.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogInfo<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.INFO"/>.
		/// Use this for happenings within expectations that are significant.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogInfo(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.DEBUG"/>.
		/// Use this for logging the flow of the system.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogDebug<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.DEBUG"/>.
		/// Use this for logging the flow of the system.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogDebug(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.TRACE"/>.
		/// Use this for the most detailed messages.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogTrace<T>(T message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message with a <see cref="LogLevel"/> of <see cref="LogLevel.TRACE"/>.
		/// Use this for the most detailed messages.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message"/> originated. </param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message"/> originated. </param>
		/// <param name="filePath">The path to the file where the <paramref name="message"/> originated. </param>
		void LogTrace(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message.
		/// </summary>
		/// <typeparam name="T">The type of the object being logged.</typeparam>
		/// <param name="message">Any object or value you wish to log. Objects will be serialized if defined in the <see cref="Configuration" />.</param>
		/// <param name="level">The <see cref="LogLevel" /> indicating the severity of this <paramref name="message" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		void Log<T>(T message, LogLevel level, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Logs a message.
		/// </summary>
		/// <param name="message">The message you wish to log.</param>
		/// <param name="level">The <see cref="LogLevel" /> indicating the severity of this <paramref name="message" />.</param>
		/// <param name="methodName">The name of the method where the <paramref name="message" /> originated.</param>
		/// <param name="lineNumber">The number of the line where the <paramref name="message" /> originated.</param>
		/// <param name="filePath">The path to the file where the <paramref name="message" /> originated.</param>
		void Log(string message, LogLevel level, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "");

		/// <summary>
		/// Checks the configuration for inconsistencies or potential problems.
		/// </summary>
		/// <returns>A list containing all messages regarding the configuration.</returns>
		ConfigurationTestResults CheckConfiguration();

		/// <summary>
		/// Starts this Logger.
		/// It is recommended <see cref="CheckConfiguration"/> is called and checked before calling this.
		/// </summary>
		/// <param name="logLoggerStarted">if set to <c>true</c> the logger will log an info entry that it was started.</param>
		void Start(bool logLoggerStarted = true);

		/// <summary>
		/// Stops this logger. Optionally flushes the log and waits for it to be fully processed.
		/// </summary>
		/// <param name="flushLog">if set to <c>true</c> flushes the log before returning.</param>
		void Stop(bool flushLog = true);
	}
}
