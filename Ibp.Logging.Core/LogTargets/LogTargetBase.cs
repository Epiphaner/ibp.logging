﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="LogTargetBase.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.LogTargets {
	using System;
	using System.Collections.Generic;
	using System.IO;
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// Depicts a logtarget that can log a list of entries.
	/// </summary>
	public abstract class LogTargetBase {
		/// <summary>
		/// The list of <see cref="LogEntry"/>'s still to be processed.
		/// </summary>
		private readonly List<LogEntry> logEntryBacklog = new List<LogEntry>();

		/// <summary>
		/// Gets or sets the last time this logtarget processed <see cref="LogEntry"/>'s.
		/// </summary>
		private DateTime lastProcessTime = DateTime.MinValue;

		/// <summary>
		/// Initializes a new instance of the <see cref="LogTargetBase"/> class.
		/// </summary>
		/// <param name="name">The name of the logtarget.</param>
		internal LogTargetBase(string name) {
			this.Name = Path.GetFileNameWithoutExtension(name ?? throw new ArgumentNullException(nameof(name)));
		}

		/// <summary>
		/// Gets the name of this logtarget.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets configuration of the logger bound to this target.
		/// </summary>
		public LoggerConfiguration LoggerConfiguration { get; internal set; }

		/// <summary>
		/// Initializes this instance.
		/// Initialization performs all one-off actions that should be performed before logging starts.
		/// </summary>
		public abstract void InitializeTarget();

		/// <summary>
		/// Finalizes this instance.
		/// Finalization performs all one-off actions that should be performed after logging is done.
		/// </summary>
		public abstract void FinalizeTarget();

		/// <summary>
		/// Log the entries to the target.
		/// </summary>
		/// <param name="entries">The list of log entries that should be processed and written. </param>
		/// <param name="force">If true, forces the logtarget to process the entries even if the interval timer has yet to expire.</param>
		public void LogInternal(IEnumerable<LogEntry> entries, bool force) {
			List<LogEntry> entriesToProcess;
			lock (this.logEntryBacklog) {
				DateTime processTime = DateTime.Now;
				TimeSpan minimumInterval = this.GetLogTargetConfiguration().LogUpdateInterval ?? this.LoggerConfiguration.LogUpdateInterval ?? this.LoggerConfiguration.DefaultLogInterval;
				int maximumLogEntriesInCache = this.GetLogTargetConfiguration().MaximumLogEntriesInCache ?? this.LoggerConfiguration.MaximumLogEntriesInCache ?? this.LoggerConfiguration.DefaultMaximumLogEntriesInCache;

				// If the minimum interval has not yet been reached, stash this batch of logentries in the backlog.
				if ((processTime - this.lastProcessTime) < minimumInterval && !force) {
					this.logEntryBacklog.AddRange(entries);

					// Only skip processing if the backlog has not exceeded the maximum size.
					if (this.logEntryBacklog.Count < maximumLogEntriesInCache)
					{
						return;
					}
				}

				// We are going to process the entries this time, so set the last processing time.
				this.lastProcessTime = processTime;
				entriesToProcess = new List<LogEntry>(this.logEntryBacklog);
				entriesToProcess.AddRange(entries);
				this.logEntryBacklog.Clear();
			}

			this.LogEntries(entriesToProcess);
		}

		/// <summary>
		/// Tests the configuration for the <see cref="LogTarget{TLogConfiguration}"/> and reports any issues.
		/// Any issues in the base configuration are tested and reported by the logger.
		/// Only issues specific to the logtarget implementation should be reported.
		/// </summary>
		/// <param name="testResults">The instance to be filled with all messages generated during the test.</param>
		public abstract void TestConfiguration(ConfigurationTestResults testResults);

		/// <summary>
		/// Gets the <see cref="LogConfigurationBase"/> for this <see cref="LogTarget{TLogConfiguration}"/>.
		/// </summary>
		/// <returns>The <see cref="LogConfigurationBase"/>.</returns>
		public abstract LogConfigurationBase GetLogTargetConfiguration();

		/// <summary>
		/// Log the entries to the target.
		/// </summary>
		/// <param name="entries">The list of log entries that should be processed and written. </param>
		internal abstract void LogEntries(ICollection<LogEntry> entries);
	}
}
