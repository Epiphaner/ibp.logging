﻿// ***********************************************************************
// Assembly         : Ibp.Logging.Core
// Author           : rene
// Created          : 03-20-2019
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="LogTarget{TLogConfiguration}.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Core.LogTargets {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.IO;
	using System.Linq;
	using System.Runtime.CompilerServices;
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// Target for writing logs.
	/// </summary>
	/// <typeparam name="TLogConfiguration">Type of log configuration.</typeparam>
	public abstract class LogTarget<TLogConfiguration> : LogTargetBase
		where TLogConfiguration : LogConfigurationBase {
		private readonly object key = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="LogTarget{TLogConfiguration}"/> class.
		/// The logtarget is automatically registered with the provided logger.
		/// </summary>
		/// <param name="configuration">Configuration for this logtarget.</param>
		/// <param name="name">The name of this logtarget.</param>
		/// <exception cref="ArgumentNullException">When <paramref name="configuration"/> is null.</exception>
		public LogTarget(TLogConfiguration configuration, [CallerFilePath] string name = null)
			: base(name) {
			this.Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
		}

		/// <summary>
		/// Gets the configuration for this <see cref="LogTarget{TLogConfiguration}"/>.
		/// </summary>
		public TLogConfiguration Configuration { get; }

		/// <inheritdoc/>
		public sealed override LogConfigurationBase GetLogTargetConfiguration() => this.Configuration;

		/// <summary>
		/// Log the entries to the target.
		/// </summary>
		/// <param name="entries">The list of log entries that should be processed and written. </param>
		internal sealed override void LogEntries(ICollection<LogEntry> entries) {
			// Don't log if the logger is not enabled.
			if (!this.Configuration.Enabled) { return; }

			// Only one thread at the time to prevent messages appearing out-of-order.
			lock (this.key) {
				// Only log when there are entries to log.
				IEnumerable<LogEntry> relevantEntries = entries.Where(entry => this.IsLogLevelAccepted(entry.Level));
				if (!relevantEntries.Any()) {
					return;
				}

				// Do the logging in a safe context to prevent exceptions from bubbeling up to the calling method.
				try {
					this.Log(relevantEntries.ToList());
				} catch (Exception exception) {
					Debug.WriteLine(exception);
					this.LoggerConfiguration.LogTargetExceptionHandler?.Invoke(exception);
				}
			}

			// Release all logentries to the pool.
			foreach (LogEntry entry in entries) {
				entry.Release();
			}
		}

		/// <summary>
		/// Formats the entry into a string according to the provided formatters in the configuration.
		/// </summary>
		/// <param name="entry">The <see cref="LogEntry"/> to format into a string.</param>
		/// <returns>A formatted string.</returns>
		protected virtual string FormatMessage(LogEntry entry) {
			(string callerPath, string methodName, int lineNumber, LogLevel level, DateTimeOffset timeStamp) = entry;
			FormatLogConfiguration formatConfiguration = this.Configuration.Format;

			// Convert the object to a usable string.
			string message = entry.GetMessage(this.GetSerializationType());

			// If a custom formatter has been defined, use it.
			if (!formatConfiguration.IsCustomFormatDefined()) {
				formatConfiguration = this.LoggerConfiguration.Format;
			}

			if (formatConfiguration.Formatter != null) { return formatConfiguration.Formatter(message, level, callerPath, methodName, lineNumber, timeStamp); }
			string executionPath = $@"{Path.GetFileNameWithoutExtension(callerPath)}->{methodName}:{lineNumber}";
			if (formatConfiguration.SimpleFormatter != null) { return formatConfiguration.SimpleFormatter(message, level, executionPath, timeStamp); }

			// Decide whether to show the log with or without timestamp.
			string log = String.Empty;
			if (formatConfiguration.ShowTimestamp) {
				log += $"[{timeStamp:yyyy-MM-dd HH:mm:ss.fff}]\t";
			}

			// Do we show the execution path?
			if (!(this.Configuration.IgnoredLogLevels.HasFlag(LogLevel.DEBUG)
				&& this.Configuration.IgnoredLogLevels.HasFlag(LogLevel.TRACE))) {
				log += $"{executionPath,-40}\t";
			}

			// Add the level and message.
			log += $":{level,-5}: {message}";

			// Return the generated log line.
			return log;
		}

		/// <summary>
		/// Logs the <paramref name="entries"/> to the desired location.
		/// </summary>
		/// <param name="entries">The entries to log.</param>
		protected abstract void Log(List<LogEntry> entries);

		/// <summary>
		/// Gets the configured type of the serialization to use.
		/// </summary>
		/// <returns>Configured serialization type.</returns>
		protected virtual SerializationType GetSerializationType() => this.Configuration.Format.SerializationType ?? this.LoggerConfiguration.Format.SerializationType ?? SerializationType.NONE;

		/// <summary>
		/// Checks if the <see cref="LogLevel"/> provided is accepted by the configuration.
		/// </summary>
		/// <param name="level">The <see cref="LogLevel"/> to check.</param>
		/// <returns>True if the <paramref name="level"/> is accepted, false otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private bool IsLogLevelAccepted(LogLevel level) => !this.Configuration.IgnoredLogLevels.HasFlag(level);
	}
}
