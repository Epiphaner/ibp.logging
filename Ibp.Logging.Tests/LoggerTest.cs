﻿// ***********************************************************************
// Assembly         : Ibp.LoggingTests
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="LoggerTest.cs" company="">
//     Copyright ©  2018
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ibp.Logging;
using Ibp.Logging.Core;
using Ibp.Logging.Core.Configuration;
using Ibp.Logging.LogTargets;
using Ibp.Logging.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ibp.LoggingTests
{
	[TestClass]
	public class LoggerTest {
		/// <summary>
		/// Tests if the logger can write messages to the console.
		/// </summary>
		[TestMethod]
		public void TestConsole() {
			var logger = new Logger();
			logger.Configuration.RegisterLogTarget(new ConsoleLogTarget());
			logger.CheckConfiguration();
			logger.Start();

			var output = new StringBuilder();

			using (TextWriter writer = new StringWriter(output)) {
				TextWriter originalOut = Console.Out;
				Console.SetOut(writer);
				logger.LogInfo("Console test");
				logger.WaitForLogToFinish();
				writer.Flush();
				Console.SetOut(originalOut);
			}

			string result = output.ToString();
			logger.Stop();

			Debug.WriteLine($"Result:\n{result}");
			StringAssert.Contains(result, "Console test", "Contents: {0}", result);
		}


		/// <summary>
		/// Tests if messages are logged in the order in which they are submitted.
		/// </summary>
		[TestMethod]
		public void TestSequentiality()
		{
			// The amount of messages to test with.
			int amount = 5000;
			// The complete input sequence.
			var input = new List<string>(this.Sequence(amount,i=>$"Iteration {i}."));
			// All outputted messages in order.
			var output = new List<string>();

			// Configure the logger.
			var logger = new Logger();
			GenericLogTarget target = logger.Configuration.RegisterLogTarget(new GenericLogTarget());

			// Format the message to purely the message so we can test on string equality.
			target.Configuration.Format.Formatter = (string message, LogLevel level, string callerPath, string methodName, int lineNumber, DateTimeOffset timeStamp) => message;
			// Handle the messages using a simple handler.
			target.Configuration.SimpleLogMethod = new GenericLogConfiguration.SimpleLogMethodHandler((message, level) => {
				// Write to the console.
				Debug.WriteLine($"From generic: {level}\t: {message}");
				// Add the message to the output.
				// This part should NOT be multi-threaded, so we don't have to account for concurrency issues.
				output.Add(message);
				// Throw in some hiccups.
				if (output.Count%(amount/10) == 0) {
					Thread.Sleep(50);
				}
			});

			// Check the config as is standard.
			ConfigurationTestResults result = logger.CheckConfiguration();
			if (result.GetMessages().Any(m=>m.Level>LogLevel.INFO)) {
				Assert.Fail("Configuration check yielded warnings or errors");
			}

			// Start the logger without the initial message.
			logger.Start(false);

			int it = 0;
			// Log all messages from the input.
			foreach (string iterationMessage in input) {
				logger.LogInfo(iterationMessage);
				// Throw in some hiccups.
				if (it++%(amount/12) == 0) {
					Thread.Sleep(50);
				}
			}

			// Stop the logger when done.
			logger.Stop();
			// Test if the input and output are equal.
			CollectionAssert.AreEqual(input, output);
		}

		private IEnumerable<string> Sequence(int count, Func<int, string> formatFunc)
		{
			for (int i = 0; i < count; i++) {
				yield return formatFunc(i);
			}
		}

		/// <summary>
		/// Tests the time it takes to send many lines to the logger.
		/// Not the time it takes to actually write those lines on the logging thread.
		/// </summary>
		[TestMethod]
		public void TestConsoleMany() {
			var logger = new Logger();
			logger.Configuration.RegisterLogTarget(new ConsoleLogTarget());
			logger.CheckConfiguration();
			logger.Start();

			TextWriter originalOut = Console.Out;
			Console.SetOut(TextWriter.Null);
			var sw = new Stopwatch();

			int totalRecordsToWrite = 500_000;
			int batchSize = 200;

			for (int batch = 0; batch < totalRecordsToWrite / batchSize; batch++) {
				// Start time measurement.
				sw.Start();
				for (int i = 0; i < batchSize; i++) {
					logger.LogInfo($"Console test #{i}");
				}
				sw.Stop();
				logger.WaitForLogToFinish();
			}
			double secondsTaken = sw.Elapsed.TotalSeconds;
			logger.Stop();

			Console.SetOut(originalOut);

			Debug.WriteLine($"Logging calls time taken: {secondsTaken} seconds");
			Assert.IsTrue(secondsTaken < 1, "The overhead for logging the messages was too high. Logging 500k messages took {0} seconds. (Threshold: 1s)", secondsTaken);
		}

		/// <summary>
		/// Tests if the logger can write JSON messages to the console.
		/// </summary>
		[TestMethod]
		public void TestConsoleJson() {
			var logger = new Logger();
			logger.Configuration.RegisterLogTarget(new ConsoleLogTarget());
			logger.Configuration.Format.SerializationType = SerializationType.JSON;
			logger.CheckConfiguration();
			logger.Start();

			var output = new StringBuilder();

			using (TextWriter writer = new StringWriter(output)) {
				TextWriter originalOut = Console.Out;
				Console.SetOut(writer);
				logger.LogInfo(logger);
				logger.WaitForLogToFinish();
				writer.Flush();
				Console.SetOut(originalOut);
			}

			logger.Stop();

			string result = output.ToString();
			Debug.WriteLine($"Result:\n{result}");
			Assert.IsTrue(true);
		}
		/// <summary>
		/// Tests if the logger can write XML messages to the console.
		/// </summary>
		[TestMethod]
		public void TestConsoleXml() {
			var logger = new Logger();
			logger.Configuration.RegisterLogTarget(new ConsoleLogTarget());
			logger.Configuration.Format.SerializationType = SerializationType.XML;
			logger.CheckConfiguration();
			logger.Start();

			var output = new StringBuilder();

			using (TextWriter writer = new StringWriter(output)) {
				TextWriter originalOut = Console.Out;
				Console.SetOut(writer);
				logger.LogInfo(logger);
				logger.WaitForLogToFinish();
				writer.Flush();
				Console.SetOut(originalOut);
			}

			logger.Stop();

			string result = output.ToString();
			Debug.WriteLine($"Result:\n{result}");
			Assert.IsTrue(true);
		}

		[TestMethod]
		public void TestGenericLogger() {
			var logger = new Logger();
			GenericLogTarget target = logger.Configuration.RegisterLogTarget(new GenericLogTarget());

			bool genericLogFired = false;
			target.Configuration.SimpleLogMethod = new GenericLogConfiguration.SimpleLogMethodHandler((message, level) => {
				Debug.WriteLine($"From generic: {level}\t: {message}");
				genericLogFired = true;
			});

			logger.CheckConfiguration();
			logger.Start();

			logger.LogInfo("TestGenericLogger");

			logger.Stop();
			Assert.IsTrue(genericLogFired, "Generic log was not triggered. ");
		}

		[TestMethod]
		public void TestWaitForSingleFinish() {
			var logger = new Logger();
			GenericLogTarget target = logger.Configuration.RegisterLogTarget(new GenericLogTarget());

			bool genericLogDone = false;
			target.Configuration.SimpleLogMethod = new GenericLogConfiguration.SimpleLogMethodHandler((message, level) => {
				Debug.WriteLine($"From generic: {level}\t: {message}");
				Thread.Sleep(5000);
				genericLogDone = true;
			});

			logger.CheckConfiguration();
			logger.Start();

			logger.LogInfo("TestWaitForSingleFinish");

			logger.Stop();
			Assert.IsTrue(genericLogDone, "Generic log was not done. ");
		}

		[TestMethod]
		public void TestWaitForMultipleFinish() {
			var logger = new Logger();
			int logTargetCount = 10;
			var targets = new List<GenericLogTarget>();
			var doneMap= new Dictionary<GenericLogTarget, bool>();

			for (int i = 0; i < logTargetCount; i++) {
				int nr = i;
				GenericLogTarget target = logger.Configuration.RegisterLogTarget(new GenericLogTarget());
				doneMap.Add(target, false);
				target.Configuration.SimpleLogMethod = new GenericLogConfiguration.SimpleLogMethodHandler((message, level) => {
					Debug.WriteLine($"From generic{nr}: {level}\t: {message}");
					Thread.Sleep(5000);
					doneMap[target] = true;
				});
			}

			logger.CheckConfiguration();
			logger.Start();

			logger.LogInfo("TestWaitForMultipleFinish");

			logger.Stop();
			Assert.IsTrue(doneMap.Values.All(v=>v), "Not all logtargets were done. ");
		}

		public void TestFileLogger() {
		}
	}
}
