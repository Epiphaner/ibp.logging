﻿namespace Ibp.Logging.Extensions.Microsoft {
	using System;

	internal class IbpLoggerScope : IDisposable {
		private readonly Action _onDisposeCallback;

		public IbpLoggerScope(Action onDisposeCallback) => this._onDisposeCallback=onDisposeCallback;

		public void Dispose() => _onDisposeCallback();
	}

	internal class IbpLoggerScope<T> : IbpLoggerScope {
		public IbpLoggerScope(Action onDisposeCallback, T state) 
			: base(onDisposeCallback) 
			=> this.State = state;

		public T State { get; private set; }
	}
}
