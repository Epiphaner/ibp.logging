﻿using Ibp.Logging.Core.Configuration;

using Microsoft.Extensions.Logging;

using System;

namespace Ibp.Logging.Extensions.Microsoft
{
	/// <summary>
	/// Class provider for IBP loggers.
	/// Implements the <see cref="ILoggerProvider" />
	/// </summary>
	/// <seealso cref="ILoggerProvider" />
	public class IbpLoggerProvider : ILoggerProvider {
		/// <summary>
		/// Gets or sets the default configuration for created loggers.
		/// </summary>
		/// <value>The configuration.</value>
		public LoggerConfiguration Configuration { get; set; } = new LoggerConfiguration();

		/// <inheritdoc/>
		public ILogger CreateLogger(string categoryName) => new IbpLogger(this.Configuration);

		/// <inheritdoc/>
		public void Dispose(){}
	}
}
