﻿namespace Ibp.Logging.Extensions.Microsoft {
	using Ibp.Logging.Core.Configuration;

	using System;
	using System.Collections.Concurrent;

	/// <summary>
	/// Implementation of <see cref="global::Microsoft.Extensions.Logging.ILogger"/> that provides superficial access to the actual logger.
	/// </summary>
	public class IbpLogger : global::Microsoft.Extensions.Logging.ILogger {
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Ibp.Logging.Extensions.Microsoft.Ibp.Logging" /> class.
		/// </summary>
		/// <param name="configuration">The configuration.</param>
		public IbpLogger(LoggerConfiguration configuration) {
			_scopes = new ConcurrentStack<IbpLoggerScope>();
			this._logger = new Ibp.Logging.Logger(configuration);
			this._logger.CheckConfiguration();
			this._logger.Start(false);
		}

		private readonly Ibp.Logging.Logger _logger;
		private readonly ConcurrentStack<IbpLoggerScope> _scopes;

		/// <inheritdoc/>
		public IDisposable BeginScope<TState>(TState state) {
			var scope = new IbpLoggerScope<TState>(() => _=this._scopes.TryPop(out _), state);
			_scopes.Push(scope);
			return scope;
		}

		/// <inheritdoc/>
		public bool IsEnabled(global::Microsoft.Extensions.Logging.LogLevel logLevel) => this._logger.Configuration.Enabled;

		/// <inheritdoc/>
		public void Log<TState>(global::Microsoft.Extensions.Logging.LogLevel logLevel, global::Microsoft.Extensions.Logging.EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) => throw new NotImplementedException();
	}
}
