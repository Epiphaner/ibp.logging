﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.File
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="FileLogConfiguration.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Configuration {
	using System;
	using System.IO;
	using System.Reflection;
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// Contains a configuration regarding logging to files.
	/// </summary>
	public class FileLogConfiguration : LogConfigurationBase {
		private string _logDirectory = GetDefaultLogDirectory();

		// file name prefix.
		private string _logFilePrefix = String.Empty;

		/// <summary>
		/// Initializes a new instance of the <see cref="FileLogConfiguration"/> class.
		/// </summary>
		public FileLogConfiguration() {
			// Set the interval to 1 second to prevent spamming the filesystem.
			this.LogUpdateInterval = TimeSpan.FromSeconds(1);
		}

		/// <summary>
		/// Method for naming the logfiles.
		/// </summary>
		/// <param name="directoryPath">Path of the logging directory. </param>
		/// <param name="prefix">Prefix for the filename. </param>
		/// <param name="part">When the log becomes too big, it is divided in parts. Part 0 is the part before the log becomes too big. </param>
		/// <param name="extension">The extension of the file. </param>
		/// <returns>A fully qualified path to the logfile. </returns>
		public delegate string LogFileNameHandler(string directoryPath, string prefix, int part, string extension);

		/// <summary>
		/// Gets or sets the directory logfiles should be written to. Will be created if it doesn't exist.
		/// </summary>
		public string LogDirectory {
			get => this._logDirectory ?? String.Empty;

			set {
				// If the directory does not exist, create it.
				if (!String.IsNullOrWhiteSpace(value) && !Directory.Exists(value)) { Directory.CreateDirectory(value); }

				if (String.IsNullOrWhiteSpace(value)) {
					// If the value is empty, revert to the default directory.
					this._logDirectory = GetDefaultLogDirectory();
				} else {
					this._logDirectory = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets custom formatter for naming the logfiles.
		/// Can be used to put them in subdirectories.
		/// </summary>
		public LogFileNameHandler LogFileNameFormatter { get; set; }

		/// <summary>
		/// Gets or sets get or set the prefix to use in the naming of files.
		/// </summary>
		public string LogFilePrefix {
			get => this._logFilePrefix;
			set => this._logFilePrefix = String.IsNullOrWhiteSpace(value) ? String.Empty : value;
		}

		/// <summary>
		/// Gets or sets the file extension/suffix to use for the logfiles.
		/// Includes the '.'.
		/// Default is ".log".
		/// </summary>
		public string LogFileExtension { get; set; } = ".log";

		/// <summary>
		/// Gets or sets how many logfiles should be kept?.
		/// </summary>
		public int MaximumLogfilesToKeep { get; set; } = 100;

		/// <summary>
		/// Gets or sets a value indicating whether should excess logfiles be cleaned periodically?.
		/// </summary>
		public bool CleanExcessLogfiles { get; set; } = true;

		/// <summary>
		/// Gets or sets the amount of time between logfile cleaning checks.
		/// Default is one hour.
		/// </summary>
		public TimeSpan CleaningInterval { get; set; } = TimeSpan.FromHours(1);

		/// <summary>
		/// Gets or sets the maximum file size in bytes that a logfile may become.
		/// Default is 10MB.
		/// </summary>
		public long MaximumLogFileSize { get; set; } = 1024 * 1024 * 10;

		/// <summary>
		/// Gets the time of the last time the log files were checked for cleaning.
		/// </summary>
		public DateTime TimeOfLastExcessLogfilesCheck { get; internal set; } = DateTime.MinValue;

		/// <summary>
		/// Gets the current part of the log.
		/// When the target logfile gets too large, it may be split into parts.
		/// This value depicts which part is the current target.
		/// </summary>
		public int CurrentLogFilePart { get; internal set; } = 0;

		/// <summary>
		/// Get the name for the current logfile. (including file extension).
		/// </summary>
		/// <returns>The name for the current logfile. (including file extension).</returns>
		public string GetLogFileName() {
			if (this.LogFileNameFormatter != null) {
				return this.LogFileNameFormatter(this.LogDirectory, this.LogFilePrefix, this.CurrentLogFilePart, this.LogFileExtension);
			} else {
				if (this.CurrentLogFilePart == 0) {
					return $@"{this._logFilePrefix}{DateTime.Now:yyyy-MM-dd}{this.LogFileExtension}";
				}

				return $@"{this._logFilePrefix}{DateTime.Now:yyyy-MM-dd} [{this.CurrentLogFilePart}]{this.LogFileExtension}";
			}
		}

		/// <summary>
		/// Get the fully qualified path to the current logfile.
		/// </summary>
		/// <returns>A fully qualified path to the current logfile. </returns>
		public string GetLogFilePath() => Path.Combine(this.LogDirectory, this.GetLogFileName());

		private static string GetDefaultLogDirectory() {
			// The default directory is a "logs" subdirectory in the directory of the executing assembly.
			string logDirName = "logs";

			try {
				// Try to get the directory of the executable.
				string codeBase = Assembly.GetExecutingAssembly().GetName().CodeBase;
				var uri = new UriBuilder(codeBase);
				string path = Uri.UnescapeDataString(uri.Path);
				return Path.Combine(Path.GetDirectoryName(path), logDirName);
			} catch (Exception) {
				// If something went wrong, cheese it with the relative directory.
				return logDirName;
			}
		}
	}
}
