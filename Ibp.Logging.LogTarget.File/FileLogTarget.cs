﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.File
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="FileLogTarget.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.LogTargets {
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using Ibp.Logging.Configuration;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.Core.LogTargets;

	/// <summary>
	/// LogTarget for logging to file.
	/// </summary>
	public class FileLogTarget : LogTarget<FileLogConfiguration>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="FileLogTarget"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to use.</param>
		public FileLogTarget(FileLogConfiguration configuration)
			: base(configuration)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FileLogTarget"/> class.
		/// Uses the default configuration.
		/// </summary>
		public FileLogTarget()
			: this(new FileLogConfiguration()) { }

		/// <inheritdoc/>
		public override void InitializeTarget()
		{
			string logDirPath = this.Configuration.LogDirectory;
			Directory.CreateDirectory(logDirPath);
		}

		/// <summary>
		/// Finalizes this instance.
		/// Finalization performs all one-off actions that should be performed after logging is done.
		/// </summary>
		public override void FinalizeTarget()
		{
			// No finalization needed.
		}

		/// <inheritdoc/>
		public override void TestConfiguration(ConfigurationTestResults testResults)
		{
			string logFilePath = this.Configuration.GetLogFilePath();
			string logDirPath = this.Configuration.LogDirectory;
			var directoryInfo = new DirectoryInfo(logDirPath);
			testResults.AddMessage(this, LogLevel.INFO, $"Logging directory: {logDirPath}");

			// Check the logging directory.
			long logDirSize = 0;
			if (!directoryInfo.Exists)
			{
				testResults.AddMessage(this, LogLevel.WARNING, "The logging directory does not exist, it will be created on initialize");
			} else {
				logDirSize = directoryInfo.EnumerateFiles($"*{this.Configuration.LogFileExtension}", SearchOption.AllDirectories).Sum(file => file.Length);
				testResults.AddMessage(this, LogLevel.INFO, $"Current logdirectory size is {this.FormatBytes(logDirSize)}");
			}

			long potentialLogDirSize = this.Configuration.MaximumLogfilesToKeep * this.Configuration.MaximumLogFileSize;
			testResults.AddMessage(this, LogLevel.INFO, $"Configured maximum logdirectory size is {this.FormatBytes(potentialLogDirSize)}");
			long bytesToPotential = potentialLogDirSize - logDirSize;
			testResults.AddMessage(this, LogLevel.INFO, $"{this.FormatBytes(bytesToPotential)} left until the configured maximum logdirectory size is reached");

			string driveName = Path.GetPathRoot(Path.GetFullPath(logDirPath));
			DriveInfo driveInfo = DriveInfo.GetDrives().FirstOrDefault(d => d.Name == driveName);
			if (driveInfo is null)
			{
				testResults.AddMessage(this, LogLevel.INFO, "Log drive information could not be inferred");
			}
			else
			{
				if (bytesToPotential > driveInfo.AvailableFreeSpace)
				{
					testResults.AddMessage(this, LogLevel.WARNING, $"Logdrive has {this.FormatBytes(driveInfo.AvailableFreeSpace)} free space, but the log can grow {this.FormatBytes(bytesToPotential)} before reaching configured maximum");
				}
			}
		}

		/// <inheritdoc/>
		protected sealed override void Log(List<LogEntry> entries)
		{
			// Check if enough time has passed since the last check for excess logs.
			if ((DateTime.Now - this.Configuration.TimeOfLastExcessLogfilesCheck) > this.Configuration.CleaningInterval)
			{
				// If enough time has passed, run the cleaner.
				Task.Run(() => this.RemoveExcessLogs(this.Configuration));
			}

			// Build the logentries into a giant string for efficient writing.
			// This saves on file access attempts and speeds up logging significantly.
			var builder = new StringBuilder();
			foreach (LogEntry entry in entries)
			{
				builder.AppendLine(this.FormatMessage(entry));
			}

			// Get the file we will write to according to the configuration.
			var file = new FileInfo(this.Configuration.GetLogFilePath());

			// And check if the maximum logfile size has already been reached.
			if (file.Exists && file.Length > this.Configuration.MaximumLogFileSize)
			{
				this.Configuration.CurrentLogFilePart++;
			}

			// Put all the entries in the file in a single transaction, this saves a lot of performance.
			File.AppendAllText(this.Configuration.GetLogFilePath(), builder.ToString());
		}

		/// <summary>
		/// Removes the oldest logs until the amount left is at most the configured maximum amount of files to keep.
		/// </summary>
		private void RemoveExcessLogs(FileLogConfiguration logConfiguration)
		{
			// Check if there are no conditions active that would prevent proper operation.
			if (!logConfiguration.Enabled || !logConfiguration.CleanExcessLogfiles || logConfiguration.MaximumLogfilesToKeep < 1) { return; }

			// We are now checking for excess logs, so update the time last checked.
			logConfiguration.TimeOfLastExcessLogfilesCheck = DateTime.Now;

			// If something goes wrong with the cleaner, we don't want to propagate that to the logger.
			try
			{
				// Get the log files from the log directory.
				IEnumerable<string> logFiles = Directory.EnumerateFiles(logConfiguration.LogDirectory, $"*{logConfiguration.LogFileExtension}");

				// Get the logs that should be removed.
				logFiles = logFiles.OrderBy(file => File.GetLastWriteTime(file)).Take(Math.Max(0, logFiles.Count() - logConfiguration.MaximumLogfilesToKeep));

				// Remove each excess log.
				foreach (string file in logFiles)
				{
					File.Delete(file);
				}
			}
			catch (Exception)
			{
				// Ignore.
			}
		}

		/// <summary>
		/// Formats the amount of bytes into a human-readable format.
		/// </summary>
		/// <param name="amount">The amount to transform.</param>
		/// <returns>A human-readable string representing the amount of bytes.</returns>
		private string FormatBytes(long amount)
		{
			long valueOrder(int order)
			{
				long value = 1;
				for (int i = 0; i < order; i++)
				{
					value *= 1000;
				}

				return value;
			}

			string[] symbolOrder = {
				"bytes",
				"kB",
				"MB",
				"GB",
				"TB",
				"PB",
				"EB",
				"ZB",
				"YB",
			};

			string prefix = String.Empty;
			if (amount < 0)
			{
				prefix = "-";
				amount = -amount;
			}

			for (int order = 0; order < symbolOrder.Length; order++)
			{
				if (amount >= valueOrder(order) && amount < valueOrder(order + 1))
				{
					return $"{prefix}{amount / (double)valueOrder(order):G3} {symbolOrder[order]}";
				}
			}

			return $"{prefix}{amount} bytes";
		}
	}
}
