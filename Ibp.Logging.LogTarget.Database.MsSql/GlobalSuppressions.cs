﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Database.MsSql
// Author           : rene
// Created          : 05-03-2019
//
// Last Modified By : rene
// Last Modified On : 05-03-2019
// ***********************************************************************
// <copyright file="GlobalSuppressions.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage(category: "StyleCop.CSharp.LayoutRules", checkId: "SA1500:Braces for multi-line statements should not share line", Justification = "Reviewed")]
[assembly: SuppressMessage(category: "StyleCop.CSharp.LayoutRules", checkId: "SA1501:StatementMustNotBeOnSingleLine", Justification = "Reviewed.")]
[assembly: SuppressMessage(category: "StyleCop.CSharp.LayoutRules", checkId: "SA1502:ElementMustNotBeOnSingleLine", Justification = "Reviewed.")]
[assembly: SuppressMessage(category: "StyleCop.CSharp.ReadabilityRules", checkId: "SA1121:UseBuiltInTypeAlias", Justification = "Reviewed.")]
[assembly: SuppressMessage(category: "StyleCop.CSharp.NamingRules", checkId: "SA1309:Field names should not begin with underscore", Justification = "Reviewed")]
