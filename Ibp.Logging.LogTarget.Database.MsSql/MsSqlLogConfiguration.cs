﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Database.MsSql
// Author           : rene
// Created          : 05-03-2019
//
// Last Modified By : rene
// Last Modified On : 05-03-2019
// ***********************************************************************
// <copyright file="MsSqlLogConfiguration.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Configuration
{
	using System;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.LogTargets;

	/// <summary>
	/// Contains a configuration for a <see cref="MsSqlLogTarget"/>.
	/// Implements the <see cref="LogConfigurationBase" />.
	/// </summary>
	/// <seealso cref="LogConfigurationBase" />
	public class MsSqlLogConfiguration : LogConfigurationBase
	{
		/// <summary>
		/// Enum ValueToMap.
		/// </summary>
		public enum ValueToMap
		{
			/// <summary>
			/// The message
			/// </summary>
			Message,

			/// <summary>
			/// The timestamp
			/// </summary>
			Timestamp,

			/// <summary>
			/// The file
			/// </summary>
			File,

			/// <summary>
			/// The linenumber
			/// </summary>
			Linenumber,

			/// <summary>
			/// The method name
			/// </summary>
			MethodName,
		}

		/// <summary>
		/// Gets or sets the connection string.
		/// </summary>
		/// <value>The connection string.</value>
		public string ConnectionString { get; set; }

		/// <summary>
		/// Gets or sets the value mapping function.
		/// </summary>
		/// <value>The value mapping function.</value>
		public Func<ValueToMap, string> ValueMapFunction { get; set; }
	}
}