﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Database.MsSql
// Author           : rene
// Created          : 05-03-2019
//
// Last Modified By : rene
// Last Modified On : 05-03-2019
// ***********************************************************************
// <copyright file="MsSqlLogTarget.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.LogTargets
{
	using System;
	using System.Collections.Generic;
	using Ibp.Logging.Configuration;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.Core.LogTargets;

	/// <summary>
	/// Class for logging to a MS SQL database.
	/// Implements the <see cref="LogTarget{T}" />.
	/// </summary>
	/// <seealso cref="LogTarget{T}" />
	public class MsSqlLogTarget : LogTarget<MsSqlLogConfiguration>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MsSqlLogTarget"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to use.</param>
		public MsSqlLogTarget(MsSqlLogConfiguration configuration)
			: base(configuration) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="MsSqlLogTarget"/> class.
		/// Uses the default configuration.
		/// </summary>
		public MsSqlLogTarget()
			: this(new MsSqlLogConfiguration()) { }

		/// <summary>
		/// Initializes this instance.
		/// Initialization performs all one-off actions that should be performed before logging starts.
		/// </summary>
		/// <exception cref="NotImplementedException">Thrown when this method is called.</exception>
		/// <inheritdoc />
		public override void InitializeTarget() => throw new NotImplementedException();

		/// <summary>
		/// Finalizes this instance.
		/// Finalization performs all one-off actions that should be performed after logging is done.
		/// </summary>
		/// <exception cref="NotImplementedException">Thrown when this method is called.</exception>
		// TODO: Implement.
		public override void FinalizeTarget() => throw new NotImplementedException();

		/// <inheritdoc/>
		public override void TestConfiguration(ConfigurationTestResults testResults) => throw new NotImplementedException();

		/// <inheritdoc/>
		protected override void Log(List<LogEntry> entries) => throw new NotImplementedException();
	}
}
