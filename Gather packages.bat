@ECHO OFF
SETLOCAL

IF NOT EXIST "Logger packages\" MKDIR "Logger packages"
DEL /S /Q "Logger packages\*"

CALL :CopyPackage "Ibp.Logging\bin\Release"
CALL :CopyPackage "Ibp.Logging.Core\bin\Release"
::CALL :CopyPackage "Ibp.Logging.Extensions.Microsoft\bin\Release"
CALL :CopyPackage "Ibp.Logging.LogTarget.Console\bin\Release"
CALL :CopyPackage "Ibp.Logging.LogTarget.Event\bin\Release"
CALL :CopyPackage "Ibp.Logging.LogTarget.File\bin\Release"
CALL :CopyPackage "Ibp.Logging.LogTarget.Generic\bin\Release"
CALL :CopyPackage "Ibp.Logging.LogTarget.Output\bin\Release"
::CALL :CopyPackage "Ibp.Logging.LogTarget.Database.MsSql\bin\Release"

GOTO :DONE

:CopyPackage
FOR /F "eol=| delims=" %%I IN ('DIR %~1"\*.nupkg" /A-D /B /O-D /TW 2^>nul') DO (
    SET NewestFile=%%I
    GOTO FoundFile
)
ECHO No *.nupkg file found in %~1!
GOTO :EOF

:FoundFile
ECHO Newest *.nupkg file is: %NewestFile%.
COPY %~1"\"%NewestFile% "Logger packages\"
EXIT /B 0

:DONE
ECHO Program finished.
ENDLOCAL
EXIT /B %ERRORLEVEL%