﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Console
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="ConsoleLogTarget.cs" company="ICT Business Partners">
// Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.LogTargets {
	using System;
	using System.Collections.Generic;
	using Ibp.Logging.Configuration;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.Core.LogTargets;

	/// <summary>
	/// <see cref="LogTargets"/> for logging to console.
	/// </summary>
	public class ConsoleLogTarget : LogTarget<ConsoleLogConfiguration> {
		/// <summary>
		/// Initializes a new instance of the <see cref="ConsoleLogTarget"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to use.</param>
		public ConsoleLogTarget(ConsoleLogConfiguration configuration)
			: base(configuration) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ConsoleLogTarget"/> class.
		/// Uses the default configuration.
		/// </summary>
		public ConsoleLogTarget()
			: this(new ConsoleLogConfiguration()) { }

		/// <inheritdoc/>
		public override void TestConfiguration(ConfigurationTestResults testResults) {
			// Check if the colors are not the same for background and forground.
			foreach (LogLevel level in Enum.GetValues(typeof(LogLevel))) {
				if (this.Configuration.GetLevelBackColor(level)  == this.Configuration.GetLevelFrontColor(level)) {
					testResults.AddMessage(this, LogLevel.WARNING, $"LogLevel {level} has same background color as foreground color");
				}
			}
		}

		/// <inheritdoc/>
		public override void InitializeTarget()
		{
			// No initialization needed.
		}

		/// <summary>
		/// Finalizes this instance.
		/// Finalization performs all one-off actions that should be performed after logging is done.
		/// </summary>
		public override void FinalizeTarget()
		{
			// No finalization needed.
		}

		/// <inheritdoc />
		protected sealed override void Log(List<LogEntry> entries) {
			ConsoleColor previousBackgroundColor = Console.BackgroundColor;
			ConsoleColor previousForegroundColor = Console.ForegroundColor;

			foreach (LogEntry entry in entries) {
				Console.BackgroundColor = this.Configuration.GetLevelBackColor(entry.Level);
				Console.ForegroundColor = this.Configuration.GetLevelFrontColor(entry.Level);
				Console.WriteLine(this.FormatMessage(entry));
			}

			Console.BackgroundColor = previousBackgroundColor;
			Console.ForegroundColor = previousForegroundColor;
		}
	}
}
