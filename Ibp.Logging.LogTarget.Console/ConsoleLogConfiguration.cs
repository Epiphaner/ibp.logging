﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Console
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="ConsoleLogConfiguration.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Configuration {
	using System;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// PContains a configuration regarding logging to the console.
	/// </summary>
	public class ConsoleLogConfiguration : LogConfigurationBase {
		private const ConsoleColor DefaultDebugBackColor  = ConsoleColor.Black;
		private const ConsoleColor DefaultDebugFrontColor = ConsoleColor.Green;
		private const ConsoleColor DefaultErrorBackColor  = ConsoleColor.Black;
		private const ConsoleColor DefaultErrorFrontColor = ConsoleColor.Red;
		private const ConsoleColor DefaultInfoBackColor   = ConsoleColor.Black;
		private const ConsoleColor DefaultInfoFrontColor  = ConsoleColor.White;
		private const ConsoleColor DefaultTraceBackColor  = ConsoleColor.Black;
		private const ConsoleColor DefaultTraceFrontColor = ConsoleColor.Cyan;
		private const ConsoleColor DefaultWarnBackColor   = ConsoleColor.Black;
		private const ConsoleColor DefaultWarnFrontColor  = ConsoleColor.Yellow;

		/// <summary>
		/// Gets or sets background color for debug messages in console.
		/// </summary>
		public ConsoleColor DebugBackColor { get; set; } = DefaultDebugBackColor;

		/// <summary>
		/// Gets or sets foreground color for debug messages in console.
		/// </summary>
		public ConsoleColor DebugFrontColor { get; set; } = DefaultDebugFrontColor;

		/// <summary>
		/// Gets or sets background color for error messages in console.
		/// </summary>
		public ConsoleColor ErrorBackColor { get; set; } = DefaultErrorBackColor;

		/// <summary>
		/// Gets or sets foreground color for error messages in console.
		/// </summary>
		public ConsoleColor ErrorFrontColor { get; set; } = DefaultErrorFrontColor;

		/// <summary>
		/// Gets or sets background color for info messages in console.
		/// </summary>
		public ConsoleColor InfoBackColor { get; set; } = DefaultInfoBackColor;

		/// <summary>
		/// Gets or sets foreground color for info messages in console.
		/// </summary>
		public ConsoleColor InfoFrontColor { get; set; } = DefaultInfoFrontColor;

		/// <summary>
		/// Gets or sets background color for trace messages in console.
		/// </summary>
		public ConsoleColor TraceBackColor { get; set; } = DefaultTraceBackColor;

		/// <summary>
		/// Gets or sets foreground color for trace messages in console.
		/// </summary>
		public ConsoleColor TraceFrontColor { get; set; } = DefaultTraceFrontColor;

		/// <summary>
		/// Gets or sets background color for warning messages in console.
		/// </summary>
		public ConsoleColor WarnBackColor { get; set; } = DefaultWarnBackColor;

		/// <summary>
		/// Gets or sets foreground color for warning messages in console.
		/// </summary>
		public ConsoleColor WarnFrontColor { get; set; } = DefaultWarnFrontColor;

		/// <summary>
		/// Get the background color for a given log level.
		/// </summary>
		/// <param name="level">The <see cref="LogLevel"/> to get the background color for.</param>
		/// <returns>The <see cref="ConsoleColor"/> in use as background for the given <see cref="LogLevel"/>.</returns>
		public ConsoleColor GetLevelBackColor(LogLevel level) {
			switch (level) {
				case LogLevel.ERROR:
					return this.ErrorBackColor;

				case LogLevel.WARNING:
					return this.WarnBackColor;

				case LogLevel.INFO:
					return this.InfoBackColor;

				case LogLevel.DEBUG:
					return this.DebugBackColor;

				case LogLevel.TRACE:
					return this.TraceBackColor;

				default:
					return ConsoleColor.Black;
			}
		}

		/// <summary>
		/// Get the foreground color for a given log level.
		/// </summary>
		/// <param name="level">The <see cref="LogLevel"/> to get the foreground color for.</param>
		/// <returns>The <see cref="ConsoleColor"/> in use as foreground for the given <see cref="LogLevel"/>.</returns>
		public ConsoleColor GetLevelFrontColor(LogLevel level) {
			switch (level) {
				case LogLevel.ERROR:
					return this.ErrorFrontColor;

				case LogLevel.WARNING:
					return this.WarnFrontColor;

				case LogLevel.INFO:
					return this.InfoFrontColor;

				case LogLevel.DEBUG:
					return this.DebugFrontColor;

				case LogLevel.TRACE:
					return this.TraceFrontColor;

				default:
					return ConsoleColor.White;
			}
		}

		/// <summary>
		/// Reset the color for debug messages to the default.
		/// </summary>
		public void ResetDebugColor() {
			this.DebugBackColor = DefaultDebugBackColor;
			this.DebugFrontColor = DefaultDebugFrontColor;
		}

		/// <summary>
		/// Reset the color for error messages to the default.
		/// </summary>
		public void ResetErrorColor() {
			this.ErrorBackColor = DefaultErrorBackColor;
			this.ErrorFrontColor = DefaultErrorFrontColor;
		}

		/// <summary>
		/// Reset the color for info messages to the default.
		/// </summary>
		public void ResetInfoColor() {
			this.InfoBackColor = DefaultInfoBackColor;
			this.InfoFrontColor = DefaultInfoFrontColor;
		}

		/// <summary>
		/// Reset the color for trace messages to the default.
		/// </summary>
		public void ResetTraceColor() {
			this.TraceBackColor = DefaultTraceBackColor;
			this.TraceFrontColor = DefaultTraceFrontColor;
		}

		/// <summary>
		/// Reset the color for warning messages to the default.
		/// </summary>
		public void ResetWarnColor() {
			this.WarnBackColor = DefaultWarnBackColor;
			this.WarnFrontColor = DefaultWarnFrontColor;
		}
	}
}
