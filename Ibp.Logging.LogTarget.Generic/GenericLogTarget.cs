﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Generic
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 05-01-2019
// ***********************************************************************
// <copyright file="GenericLogTarget.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.LogTargets {
	using System.Collections.Generic;
	using System.Linq;

	using Ibp.Logging.Configuration;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;
	using Ibp.Logging.Core.LogTargets;

	/// <summary>
	/// Logtarget for logging to delegated/generic targets.
	/// </summary>
	public class GenericLogTarget : LogTarget<GenericLogConfiguration> {
		/// <summary>
		/// Initializes a new instance of the <see cref="GenericLogTarget"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to use.</param>
		public GenericLogTarget(GenericLogConfiguration configuration)
			: base(configuration) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="GenericLogTarget"/> class.
		/// Uses the default configuration.
		/// </summary>
		public GenericLogTarget()
			: this(new GenericLogConfiguration()) { }

		/// <inheritdoc/>
		public override void TestConfiguration(ConfigurationTestResults testResults)
		{
			string ignoredText = " but will be ignored because a better candidate has been defined";

			// Test if a logging method has been defined.
			bool methodDefined = false;
			if (this.Configuration.BatchLogMethod != null)
			{
				testResults.AddMessage(this, LogLevel.INFO, "A batch logMethod has been defined");
				methodDefined = true;
			}

			if (this.Configuration.LogMethod != null)
			{
				testResults.AddMessage(this, LogLevel.INFO, "A normal logMethod has been defined" + (methodDefined ? ignoredText : System.String.Empty));
				methodDefined = true;
			}

			if (this.Configuration.SimpleBatchLogMethod != null)
			{
				testResults.AddMessage(this, LogLevel.INFO, "A simple batch logMethod has been defined" + (methodDefined ? ignoredText : System.String.Empty));
				methodDefined = true;
			}

			if (this.Configuration.SimpleLogMethod != null)
			{
				testResults.AddMessage(this, LogLevel.INFO, "A simple logMethod has been defined" + (methodDefined ? ignoredText : System.String.Empty));
				methodDefined = true;
			}

			if (!methodDefined)
			{
				testResults.AddMessage(this, LogLevel.ERROR, "No logging methods have been defined");
			}
		}

		/// <inheritdoc/>
		public override void InitializeTarget()
		{
			// Nothing to initialize.
		}

		/// <summary>
		/// Finalizes this instance.
		/// Finalization performs all one-off actions that should be performed after logging is done.
		/// </summary>
		public override void FinalizeTarget()
		{
			// No finalization needed.
		}

		/// <inheritdoc/>
		protected sealed override void Log(List<LogEntry> entries) {
			if (this.Configuration.BatchLogMethod != null) {
				// If the normal batch logging method is provided, feed it all entries in the right format at once.
				this.Configuration.BatchLogMethod(entries.AsParallel().AsOrdered().Select(entry => (entry.GetMessage(this.GetSerializationType()), entry.CallerPath, entry.MethodName, entry.LineNumber, entry.Level, entry.TimeStamp)));
			} else if (this.Configuration.LogMethod != null) {
				// If no normal batch logging method is provided
				// but a normal non-batch logging method, feed it all entries in the right format one by one.
				foreach (LogEntry entry in entries) {
					this.Configuration.LogMethod(entry.GetMessage(this.GetSerializationType()), entry.CallerPath, entry.MethodName, entry.LineNumber, entry.Level, entry.TimeStamp);
				}
			} else if (this.Configuration.SimpleBatchLogMethod != null) {
				// If no normal batch logging method or normal non-batch logging method is provided
				// but a simple batch logging method, feed it all entries in the right format at once.
				this.Configuration.SimpleBatchLogMethod(entries.AsParallel().AsOrdered().Select(entry => (this.FormatMessage(entry), entry.Level)));
			} else if (this.Configuration.SimpleLogMethod != null) {
				// If no normal batch logging method, normal non-batch logging method or simple batch logging method is provided
				// but a simple non-batch logging method, feed it all entries in the right format one by one.
				foreach (LogEntry entry in entries) {
					this.Configuration.SimpleLogMethod(this.FormatMessage(entry), entry.Level);
				}
			} else {
				// No logging methods have been provided, so we do nothing.
				// This could actually be counted as an erronous state.
				// I'm on the fence whether or not an Exception should be thrown here.
			}
		}
	}
}
