﻿// ***********************************************************************
// Assembly         : Ibp.Logging.LogTarget.Generic
// Author           : rene
// Created          : 10-27-2018
//
// Last Modified By : rene
// Last Modified On : 10-31-2018
// ***********************************************************************
// <copyright file="GenericLogConfiguration.cs" company="ICT Business Partners">
//     Copyright (c) ICT Business Partners. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Ibp.Logging.Configuration {
	using System;
	using System.Collections.Generic;
	using Ibp.Logging.Core;
	using Ibp.Logging.Core.Configuration;

	/// <summary>
	/// Contains a configuration for logging to a user-defined target.
	/// When logging, the most comprehensive method defined will be used.
	/// The priority is:
	/// <list type="number">
	///    <item>
	///        <description><see cref="BatchLogMethod"/></description>
	///    </item>
	///    <item>
	///        <description><see cref="LogMethod"/></description>
	///    </item>
	///    <item>
	///        <description><see cref="SimpleBatchLogMethod"/></description>
	///    </item>
	///    <item>
	///        <description><see cref="SimpleLogMethod"/></description>
	///    </item>
	/// </list>
	/// </summary>
	public class GenericLogConfiguration : LogConfigurationBase {
		/// <summary>
		/// Provides a method for handling a log message.
		/// </summary>
		/// <param name="message">The message to log. </param>
		/// <param name="level">The severity level of the message. </param>
		public delegate void SimpleLogMethodHandler(string message, LogLevel level);

		/// <summary>
		/// Provides a method for handling log messages in batch.
		/// </summary>
		/// <param name="entries">All messages and their levels to log. </param>
		public delegate void SimpleBatchLogMethodHandler(IEnumerable<(string message, LogLevel level)> entries);

		/// <summary>
		/// Provides a method for handling a log message.
		/// </summary>
		/// <param name="message">The message to log. </param>
		/// <param name="callerPath">Filepath of the calling class. </param>
		/// <param name="methodName">Name of the calling method. </param>
		/// <param name="lineNumber">Line number at which the logger was called. </param>
		/// <param name="level">The severity level of the message. </param>
		/// <param name="timeStamp">Time at which the logger was called. </param>
		public delegate void LogMethodHandler(string message, string callerPath, string methodName, int lineNumber, LogLevel level, DateTimeOffset timeStamp);

		/// <summary>
		/// Provides a method for handling log messages in batch.
		/// </summary>
		/// <param name="entries">All messages and their levels to log. </param>
		public delegate void BatchLogMethodHandler(IEnumerable<(string message, string callerPath, string methodName, int lineNumber, LogLevel level, DateTimeOffset timeStamp)> entries);

		/// <summary>
		/// Gets or sets the simple method for logging a message.
		/// If logging multiple messages at the same time provides a performance increase, use <see cref="SimpleBatchLogMethod"/> instead.
		/// Keep in mind that this method will be called from a seperate thread.
		/// Will not be used if <see cref="BatchLogMethod"/>, <see cref="LogMethod"/> or <see cref="SimpleBatchLogMethod"/> is defined.
		/// </summary>
		public SimpleLogMethodHandler SimpleLogMethod { get; set; }

		/// <summary>
		/// Gets or sets the simple method for logging a batch of messages.
		/// If logging multiple messages at the same time does not provide a performance increase, use <see cref="SimpleLogMethod"/> instead.
		/// Keep in mind that this method will be called from a seperate thread.
		/// Will not be used if <see cref="BatchLogMethod"/> or <see cref="LogMethod"/> is defined.
		/// </summary>
		public SimpleBatchLogMethodHandler SimpleBatchLogMethod { get; set; }

		/// <summary>
		/// Gets or sets the method for logging a message.
		/// If logging multiple messages at the same time provides a performance increase, use <see cref="BatchLogMethod"/> instead.
		/// Keep in mind that this method will be called from a seperate thread.
		/// Will not be used if <see cref="BatchLogMethod"/> is defined.
		/// </summary>
		public LogMethodHandler LogMethod { get; set; }

		/// <summary>
		/// Gets or sets the method for logging a batch of messages.
		/// If logging multiple messages at the same time does not provide a performance increase, use <see cref="LogMethod"/> instead.
		/// Keep in mind that this method will be called from a seperate thread.
		/// </summary>
		public BatchLogMethodHandler BatchLogMethod { get; set; }
	}
}
